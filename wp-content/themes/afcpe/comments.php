<?php
	// Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if ( post_password_required() ) { ?>
		<p class="nocomments">
			This post is password protected. Enter the password to view comments.
		</p>
	<?php
		return;
	}
?>
<?php if ( have_comments() ) : ?>
	<div class="comments-wrap">
		<h3 id="comments" class="goudy">
			<em><?php comments_number('No Comments', 'One Comment', '% Comments' );?></em>
		</h3>
		<div class="navigation">
			<div class="alignleft">
				<?php previous_comments_link() ?>
			</div><!-- .alignleft -->
			<div class="alignright">
				<?php next_comments_link() ?>
			</div><!-- .alignright -->
		</div><!-- .navigation -->
		<ul class="commentlist">
			<?php wp_list_comments(); ?>
		</ul><!-- .commentlist -->
		<div class="navigation">
			<div class="alignleft">
				<?php previous_comments_link() ?>
			</div><!-- .alignleft -->
			<div class="alignright">
				<?php next_comments_link() ?>
			</div><!-- .alignright -->
		</div><!-- .navigation -->
	</div><!-- .well -->
	<?php else : // this is displayed if there are no comments so far ?>
		<?php if ( comments_open() ) : ?>
		<?php else : // comments are closed ?>
		<?php endif; ?>
	<?php endif; ?>
	<?php if ( comments_open() ) : ?>
		<div class="well">
			<div id="respond" class="comments_wrap">
				<h3 class="goudy">
					<?php comment_form_title( 'Leave a Comment', 'Leave a Comment to %s' ); ?>
				</h3>
				<div class="cancel-comment-reply">
					<?php cancel_comment_reply_link(); ?>
				</div><!-- .cancel-comment-reply -->
				<?php if ( !is_user_logged_in() ) : ?>
					<p>
						You must be <a href="<?php echo wp_login_url( get_permalink() ); ?>">logged in</a> to post a comment.
					</p>
					<?php else : ?>
					<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">

						<?php if ( is_user_logged_in() ) : ?>
							<p>
								Logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Log out of this account">Log out &raquo;</a>
							</p>
							<?php else : ?>
						<?php endif; ?>
						<p>Usable tags: <code><?php echo allowed_tags(); ?></code></p>
						<p>
							<textarea class="comments" name="comment" id="comment" rows="10" tabindex="4" style="width: 100%;"></textarea>
						</p>
						<p>
							<input name="submit" type="submit" id="submit" class="afcpe-btn blue-btn goudy" tabindex="5" value="Submit" />
							<?php comment_id_fields(); ?>
						</p>
						<?php do_action('comment_form', $post->ID); ?>
					</form>
				<?php endif; // If not logged in ?>
			</div><!-- #respond -->
		</div><!-- .well ->
	<?php endif; // if you delete this the sky will fall on your head ?>