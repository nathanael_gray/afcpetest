<?php
/*
Template Name: Membership
*/
get_header(); ?>

<?php if ( have_posts() ) : ?>

	<?php the_field('page_slider'); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" class="entry post-entry">

			<?php get_template_part('blocks/content/content', 'breadcrumbs'); ?>
			<div class="membership intro-copy">
				<?php the_field('intro_copy'); ?>
			</div><!-- .intro-copy -->

			<div class="benefits-wrap section-inner">
				<div class="container">
					<div class="col-md-6">
						<div class="video-container">
							<?php the_field('video'); ?>
						</div><!-- .video-container -->
					</div><!-- .col-md-6 -->
					<div class="col-md-6">
						<div class="benefits">
							<?php the_field('benefits'); ?>
							<a href="<?php the_field('button_link'); ?>" class="afcpe-btn goudy">
								<?php the_field('button_text'); ?>
							</a>
						</div><!-- .benefits -->
					</div><!-- .col-md-6 -->
					<div class="clear"></div><!-- .clear -->
				</div><!-- .container -->
			</div><!-- .section-inner -->

			<div class="section white-bg">
				<div class="container">
					<h2 class="section-head goudy">
						<em><?php the_field('memberships_section_title'); ?></em>
					</h2>
					<hr class="dark">

					<div class="inner-section full-width">

						<div class="inner-title-wrap">
							<h3 class="goudy">
								<em><?php the_field('membership_types_title'); ?></em>
							</h3>
						</div><!-- .inner-title-wrap -->

						<div class="gray-box-wrap col-4">
							<?php if( get_field('box_1_title') ) { ?>
								<div class="box">
									<div class="inner first">
										<a href="<?php the_field('box_1_link'); ?>" class="title">
											<?php the_field('box_1_title'); ?>
										</a>
										<p class="top">
											<?php the_field('box_1_description'); ?>
										</p>
										<hr class="dark">
										<p class="bottom">
											<?php the_field('box_1_price'); ?>
										</p>
										<hr class="dark">
									</div><!-- .inner -->
								</div><!-- .box -->
							<?php } else { ?>
								<div class="box">
									<div class="inner first" style="background: transparent !important;"></div>
								</div><!-- .box -->
							<?php } ?>

							<?php if( get_field('box_2_title') ) { ?>
								<div class="box">
									<div class="inner second">
										<a href="<?php the_field('box_2_link'); ?>" class="title">
											<?php the_field('box_2_title'); ?>
										</a>
										<p class="top">
											<?php the_field('box_2_description'); ?>
										</p>
										<hr class="dark">
										<p class="bottom">
											<?php the_field('box_2_price'); ?>
										</p>
										<hr class="dark">
									</div><!-- .inner -->
								</div><!-- .box -->
							<?php } else { ?>
								<div class="box">
									<div class="inner second" style="background: transparent !important;"></div>
								</div><!-- .box -->
							<?php } ?>

							<?php if( get_field('box_3_title') ) { ?>
								<div class="box">
									<div class="inner third">
										<a href="<?php the_field('box_3_link'); ?>" class="title">
											<?php the_field('box_3_title'); ?>
										</a>
										<p class="top">
											<?php the_field('box_3_description'); ?>
										</p>
										<hr class="dark">
										<p class="bottom">
											<?php the_field('box_3_price'); ?>
										</p>
										<hr class="dark">
									</div><!-- .inner -->
								</div><!-- .box -->
							<?php } else { ?>
								<div class="box">
									<div class="inner third" style="background: transparent !important;"></div>
								</div><!-- .box -->
							<?php } ?>

							<?php if( get_field('box_4_title') ) { ?>
								<div class="box">
									<div class="inner last">
										<a href="<?php the_field('box_4_link'); ?>" class="title">
											<?php the_field('box_1_title'); ?>
										</a>
										<p class="top">
											<?php the_field('box_4_description'); ?>
										</p>
										<hr class="dark">
										<p class="bottom">
											<?php the_field('box_4_price'); ?>
										</p>
										<hr class="dark">
									</div><!-- .inner -->
								</div><!-- .box -->
							<?php } else { ?>
								<div class="box">
									<div class="inner last" style="background: transparent !important;"></div>
								</div><!-- .box -->
							<?php } ?>

							<div class="clear"></div><!-- .clear -->
						</div><!-- .gray-box-wrap -->

						<div class="below-memberships">
							<?php the_field('below_membership_types'); ?>
						</div><!-- .below-memberships -->

					</div><!-- .inner-section .full-width -->
				</div><!-- .container -->
			</div><!-- .section .white-bg -->

			<div class="section dark-gray-bg">
				<div class="container">
					<div class="inner-section full-width">
						<div class="testimonial-wrap">
							<h2 class="testimonial-head">
								<?php the_field('testimonial_section_head'); ?>
							</h2>
							<div class="slider-wrap">
								<?php the_field('testimonial_slider'); ?>
							</div><!-- .slider-wrap -->
							<center>
								<a href="<?php the_field('testimonial_button_link'); ?>" class="blue-btn goudy">
									<?php the_field('testimonial_button_text'); ?>
								</a>
							</center>
						</div><!-- .testimonials-wrap -->
					</div><!-- .inner-section .full-width -->
				</div><!-- .container -->
			</div><!-- .section .white-bg -->

			<?php get_template_part('blocks/content/subscribe', 'cta'); ?>

		</article><!-- .entry -->
	<?php endwhile; ?>
	<?php else : ?>
	<?php get_template_part('blocks/content/loop', 'nocontent'); ?>
<?php endif; ?>
<?php get_footer(); ?>