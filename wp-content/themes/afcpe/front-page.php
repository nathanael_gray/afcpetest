<?php
/*
Template Name: Home Page
*/
get_header(); ?>
<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php the_field('page_slider'); ?>
		<article id="post-<?php the_ID(); ?>" class="entry post-entry">

			<div class="intro-copy">
				<?php the_field('intro_copy'); ?>
			</div><!-- .intro-copy -->

			<div class="content-blocks-wrap">
				<div class="content-block">
					<a href="<?php the_field('block_1_link'); ?>">
						<img src="<?php the_field('block_1_image'); ?>" class="img-responsive" />
					</a>
					<h2 class="section-head home-box-title goudy">
						<?php the_field('block_1_title'); ?>
					</h2>
					<p>
						<?php the_field('block_1_content'); ?>
					</p>
					<a href="<?php the_field('block_1_link'); ?>" class="learn-more goudy">
						Learn More
					</a>
				</div><!-- .content-block -->

				<div class="content-block">
					<a href="<?php the_field('block_2_link'); ?>">
						<img src="<?php the_field('block_2_image'); ?>" class="img-responsive" />
					</a>
					<h2 class="section-head home-box-title goudy">
						<?php the_field('block_2_title'); ?>
					</h2>
					<p>
						<?php the_field('block_2_content'); ?>
					</p>
					<a href="<?php the_field('block_2_link'); ?>" class="learn-more goudy">
						Learn More
					</a>
				</div><!-- .content-block -->

				<div class="content-block">
					<a href="<?php the_field('block_3_link'); ?>">
						<img src="<?php the_field('block_3_image'); ?>" class="img-responsive" />
					</a>
					<h2 class="section-head home-box-title goudy">
						<?php the_field('block_3_title'); ?>
					</h2>
					<p>
						<?php the_field('block_3_content'); ?>
					</p>
					<a href="<?php the_field('block_3_link'); ?>" class="learn-more goudy">
						Learn More
					</a>
				</div><!-- .content-block -->
				<div class="clear"></div><!-- .clear -->
			</div><!-- .content-blocks-wrap -->

			<div class="news-trends-wrap">
				<div class="container">
					<h2 class="section-head goudy">
						<em>News & Trends</em>
					</h2>
					<hr class="dark">
					<div class="news-trends">
						<div class="featured-post">
							<h3 class="goudy">
								<em><?php the_field('blog_section_title'); ?></em>
							</h3>
							<?php $post_object = get_field('featured_post'); ?>
							<?php if( $post_object ): 
								// override $post
								$post = $post_object;
								setup_postdata( $post );
							?>
								<?php if ( has_post_thumbnail() ) : ?>
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
										<?php the_post_thumbnail( 'full', array( 'class' => 'featured-image archive-featured-image' ) ); ?>
									</a>
								<?php endif; ?>
								<p>
									<a href="<?php the_permalink(); ?>" class="featured-post-link">
										<?php the_title(); ?>
									</a>
									<br />
									By <?php the_author(); ?>
								</p>
								<p>
									<?php echo get_the_excerpt(); ?> <a href="<?php the_permalink(); ?>">Read more</a>
								</p>
								<ul class="featured-post-social">
									<li>
										<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank">
											<img src="<?php echo esc_url( home_url() ); ?>/wp-content/themes/afcpe/img/facebook.png" />
										</a>
									</li>
									<li>
										<a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink() ?>&title=<?php the_title(); ?>&summary=&source=<?php bloginfo('name'); ?>" target="_blank">
											<img src="<?php echo esc_url( home_url() ); ?>/wp-content/themes/afcpe/img/linkedin.png" />
										</a>										
									</li>
									<li>
										<script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>
										<a href="http://twitter.com/share" data-url="<?php the_permalink(); ?>" data-via="<?php bloginfo('name'); ?>" data-text="<?php the_title(); ?>" target="_blank">
											<img src="<?php echo esc_url( home_url() ); ?>/wp-content/themes/afcpe/img/twitter.png" />
										</a>
									</li>
								</ul>
								<?php wp_reset_postdata(); ?>
							<?php endif; ?>
						</div>
						<div class="col-md-3">
							<h3 class="goudy">
								<em><?php the_field('news_section_title'); ?></em>
							</h3>
							<?php
								$args=array(
									'post_type' => 'post',
									'post_status' => 'publish',
									'posts_per_page' => 3,
									'cat' => get_field('news_section')
								);
							?>
							<?php $my_query = null; ?>
							<?php $my_query = new WP_Query($args); ?>
							<?php if( $my_query->have_posts() ) { ?>
								<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
									<p>
										<a href="<?php the_permalink(); ?>"><?php the_time('F j, Y'); ?>:</a> <?php the_title(); ?>
									</p>
								<?php endwhile; } ?>
							<?php wp_reset_query(); ?>
							<a href="index.php?cat=<?php the_field('news_section'); ?>">See more</a>
						</div><!-- .col-md-3 -->
						<div class="col-md-3">
							<h3 class="goudy">
								<em><?php the_field('featured_in_section_title'); ?></em>
							</h3>
							<?php
								$args=array(
									'post_type' => 'post',
									'post_status' => 'publish',
									'posts_per_page' => 3,
									'cat' => get_field('afcpe_as_featured_in_section')
								);
							?>
							<?php $my_query = null; ?>
							<?php $my_query = new WP_Query($args); ?>
							<?php if( $my_query->have_posts() ) { ?>
								<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
									<p>
										<?php if( get_field('published_in_title') ) { ?>
											<strong><a href="<?php the_permalink(); ?>"><?php the_field('published_in_title'); ?></a></strong> 
										<?php } ?>
										<?php the_title(); ?>
									</p>
								<?php endwhile; } ?>
							<?php wp_reset_query(); ?>
							<a href="index.php?cat=<?php the_field('afcpe_as_featured_in_section'); ?>">See more</a>
						</div>
						<div class="clear"></div><!-- .clear -->
					</div><!-- .news-trends -->
				</div><!-- .container -->
			</div><!-- .news-trends-wrap -->

			<?php get_template_part('blocks/loops/events', 'loop'); ?>

		</article><!-- .entry -->
	<?php endwhile; ?>
	<?php else : ?>
	<?php get_template_part('blocks/loops/loop', 'nocontent'); ?>
<?php endif; ?>
<?php get_footer(); ?>