<?php
/*
Template Name: About AFCPE
*/
get_header(); ?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<article id="post-<?php the_ID(); ?>" class="entry post-entry">
			<?php the_field('timeline_embed'); ?>
			<div class="section white-bg">
				<div class="about-intro-copy">
					<?php the_field('intro_copy'); ?>
				</div><!-- .intro-copy -->
				<div class="inner-section full-width">
					<div class="content-wrap">
						<?php the_field('content'); ?>
					</div><!-- .content-wrap -->
					<div class="standards-wrap">
						<hr class="dark">
						<div class="standards">
							<h2 class="standards-title">
								<?php the_field('standards_title'); ?>
							</h2>
							<P class="standard">
								<?php the_field('standards'); ?>
							</P>
						</div><!-- .standards -->
						<hr class="dark">
					</div><!-- .standards-wrap -->
					<div class="content-wrap">
						<?php the_field('lower_content'); ?>
					</div><!-- .content-wrap -->
					<div class="button-wrap">
						<div class="col-md-6">
							<a href="<?php the_field('left_button_link'); ?>" class="blue-btn blue-btn-left goudy">
								<?php the_field('left_button_text'); ?>
							</a>
						</div><!-- .col-md-6 -->
						<div class="col-md-6">
							<a href="<?php the_field('right_button_link'); ?>" class="blue-btn goudy">
								<?php the_field('right_button_text'); ?>
							</a>
						</div><!-- .col-md-6 -->
						<div class="clear"></div><!-- .clear -->
					</div><!-- .button-wrap -->
				</div><!-- .inner-section .full-width -->
			</div><!-- .section .white-bg -->
			<div class="section darker-gray-bg">
				<div class="wider">
					<div class="stats-wrap">
						<center>
							<?php if( have_rows('stats') ): ?>
								<?php while ( have_rows('stats') ) : the_row(); ?>
									<div class="stat">
										<div class="inner-stat">
											<h1 class="number">
												<?php the_sub_field('stat_number'); ?>
											</h1>
											<h2 class="stats-title">
												<?php the_sub_field('stat_title'); ?>
											</h2>
											<div class="stats-description">
												<?php the_sub_field('stat_content'); ?>
											</div><!-- .stats-description -->
										</div><!-- .inner-stat -->
									</div><!-- .stat -->
								<?php endwhile; ?>
								<?php else : ?>
							<?php endif; ?>
						</center>
					</div><!-- .stats-wrap -->
				</div><!-- .inner-section .full-width -->
			</div><!-- .section .gray-bg -->
		</article><!-- .entry -->
	<?php endwhile; ?>
	<?php else : ?>
	<?php get_template_part('blocks/content/loop', 'nocontent'); ?>
<?php endif; ?>
<?php get_footer(); ?>