<?php
/*
Template Name: Career Center
*/
get_header(); ?>

<?php if ( have_posts() ) : ?>
	<?php the_field('page_slider'); ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<article id="post-<?php the_ID(); ?>" class="entry post-entry">
			<?php get_template_part('blocks/content/content', 'breadcrumbs'); ?>
			<div class="section white-bg">
				<div class="about-intro-copy">
					<?php the_field('intro_copy'); ?>
				</div><!-- .intro-copy -->
				<div class="inner-section full-width">

					<div class="button-wrap">
						<div class="col-md-6">
							<a href="<?php the_field('left_button_link'); ?>" class="blue-btn blue-btn-left goudy">
								Find a Job (43 listings)
							</a>
						</div><!-- .col-md-6 -->
						<div class="col-md-6">
							<a href="<?php the_field('right_button_link'); ?>" class="blue-btn goudy">
								Post a Job (Employers)
							</a>
						</div><!-- .col-md-6 -->
						<div class="clear"></div><!-- .clear -->
					</div><!-- .button-wrap -->

				</div><!-- .inner-section .full-width -->
			</div><!-- .section .white-bg -->

			<div class="section gray-bg">
				<div class="container">
					<h2 class="section-head goudy">
						<em><?php the_field('career_paths_section_title'); ?></em>
					</h2>
					<hr class="dark">

					<div class="content-wrap">
						<?php the_field('career_paths_content_summary'); ?>
					</div><!-- .content-wrap -->

					<div class="paths-wrap">

						<?php if( have_rows('career_paths_section') ): ?>

							<?php while ( have_rows('career_paths_section') ) : the_row(); ?>

								<?php $term = get_sub_field('career_paths_tax'); ?>
								<?php if( $term ): ?>
									<div class="toggle-container">
										<div class="section-toggle section-toggle-up gray-bg-head">
											<h3 class="goudy">
												<em><?php echo $term->name; ?></em>
											</h3>
										</div>
									</div>
								<?php endif; ?>

								<?php
									$args = array(
										'post_type' => 'career_path',
										'post_status' => 'publish',
										'posts_per_page' => 4,
										'tax_query' => array(
											array(
												'taxonomy' => 'career_path_category',
												'field' => 'slug',
												'terms' => get_sub_field('career_paths_tax')
											)
										)
									);
									$my_query = null;
									$my_query = new WP_Query($args);
								?>
								<?php if( $my_query->have_posts() ) { ?>
									<div class="toggled-container">
										<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
											<div class="gray-box-wrap col-md-3">
												<div class="box">
													<div class="inner">
														<h3 class="box-title">
															<?php the_title(); ?>
														</h3>
														<div class="toggle-box-lower">
															<div class="box-excerpt">
																<?php echo get_the_excerpt(); ?>
																<br />
																<a href="<?php the_permalink(); ?>" class="excerpt-link">
																	Learn more
																</a>
															</div><!-- .box-excerpt -->
															<hr class="dark">
															<p class="requirements">
																<strong>Requirements:</strong>
																<br />
																<?php the_field('requirements'); ?>
															</p>
														</div><!-- .toggle-box-lower -->
													</div><!-- .inner .first -->
												</div><!-- .box -->
											</div><!-- .col-md-3 -->
										<?php endwhile; } ?>
										<div class="clear"></div><!-- .clear -->
									</div><!-- .toggled-container -->
								<?php wp_reset_query(); ?>
							<?php endwhile; ?>
							<div class="clear"></div><!-- .clear -->
							<?php else : ?>
						<?php endif; ?>

					</div><!-- .paths-wrap -->

				</div><!-- .container -->

			</div><!-- .section .gray-bg -->

			<div class="section white-bg">
				<div class="inner-section">
					<div class="container">
						<div class="col-md-6">
							<h2 class="section-head goudy">
								<em><?php the_field('mentor_program_title'); ?></em>
							</h2>
							<hr class="dark">
							<?php the_field('mentor_program_contant'); ?>
							<?php if( get_field('add_mentor_button') ) { ?>
								<a class="afcpe-btn blue-btn goudy" href="<?php the_field('mentor_button_link'); ?>">
									<em><?php the_field('mentor_button_text'); ?></em>
								</a>
							<?php } ?>
						</div><!-- .col-md-6 -->
						<div class="col-md-6">
							<h2 class="section-head goudy">
								<em><?php the_field('afcpe_achieve_title'); ?></em>
							</h2>
							<hr class="dark">
							<?php the_field('afcpe_achieve_content'); ?>
							<?php if( get_field('add_afcpe_achieve_button') ) { ?>
								<a class="afcpe-btn blue-btn goudy pull-right" href="<?php the_field('afcpe_achieve_button_link'); ?>">
									<em><?php the_field('afcpe_achieve_button_text'); ?></em>
								</a>
							<?php } ?>
						</div><!-- .col-md-6 -->
						<div class="clear"></div><!-- .clear -->
					</div><!-- .container -->
				</div><!-- .inner-section .full-width -->
			</div><!-- .section .white-bg -->

		</article><!-- .entry -->
	<?php endwhile; ?>
	<?php else : ?>
	<?php get_template_part('blocks/content/loop', 'nocontent'); ?>
<?php endif; ?>
<?php get_footer(); ?>