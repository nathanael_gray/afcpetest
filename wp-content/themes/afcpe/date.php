<?php get_header(); ?>
<?php get_template_part('blocks/content/content', 'breadcrumbs'); ?>
<div class="container">
	<h1>
		<?php the_date(); ?>
	</h1>
</div><!-- .container -->
<?php get_template_part('blocks/content/loop', 'archive'); ?>
<?php get_footer(); ?>