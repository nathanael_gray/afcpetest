<?php
/*
Template Name: Certification Center
*/
get_header(); ?>
<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php the_field('page_slider'); ?>
		<article id="post-<?php the_ID(); ?>" class="entry post-entry">
			<?php get_template_part('blocks/content/content', 'breadcrumbs'); ?>
			<div class="certification intro-copy">
				<?php the_field('intro_copy'); ?>
			</div><!-- .certification .intro-copy -->
			<div class="benefits-wrap section-inner">
				<div class="container">
					<div class="col-md-6">
						<div class="video-container">
							<?php the_field('video'); ?>
						</div><!-- .video-container -->
					</div><!-- .col-md-6 -->
					<div class="col-md-6">
						<div class="benefits">
							<?php the_field('benefits'); ?>
							<a href="<?php the_field('button_link'); ?>" class="afcpe-btn goudy">
								<?php the_field('button_text'); ?>
							</a>
						</div><!-- .benefits -->
					</div><!-- .col-md-6 -->
					<div class="clear"></div><!-- .clear -->
				</div><!-- .container -->
			</div><!-- benefits-wrap -->
			<div class="programs-wrap section gray-bg">
				<div class="container">
					<h2 class="section-head goudy">
						<em><?php the_field('certification_programs_title'); ?></em>
					</h2>
					<hr class="dark">
					<center>
						<div class="image-wrap">
							<img src="<?php the_field('image'); ?>" class="img-responsive" />
						</div><!-- .image-wrap -->
					</center>
					<div class="program-intro">
						<?php the_field('program_intro_content'); ?>
					</div><!-- .program-intro -->
					<div class="programs-wrap">
						<div class="left">
							<div class="program-head">
								<h3 class="goudy">
									<em><?php the_field('counseling_specialties_title'); ?></em>
								</h3>
							</div><!-- .programs-head -->
							<div class="program-box">
								<?php if( get_field('counseling_box_1') ) { ?>
									<div class="inner first">
										<?php $post_object = get_field('counseling_box_1'); ?>
										<?php if( $post_object ): 
											$post = $post_object;
											setup_postdata( $post ); 
										?>
											<a href="<?php the_permalink(); ?>" class="program-title-link">
												<?php the_title(); ?>
											</a>
											<p>
												<?php the_excerpt(); ?>
											</p>
											<a href="<?php the_permalink(); ?>">
												Learn more
											</a>
										    <?php wp_reset_postdata(); ?>
										<?php endif; ?>

										<hr class="dark">
										<p>
											<?php the_field('counseling_box_1_requirements'); ?>
										</p>
									</div><!-- .inner -->
								<?php } else { ?>
									<div class="inner first" style="background: transparent !important;"></div><!-- .inner -->
								<?php } ?>
							</div><!-- .program-box -->
							<div class="program-box">
								<?php if( get_field('counseling_box_2') ) { ?>
									<div class="inner">
										<?php $post_object = get_field('counseling_box_2'); ?>
										<?php if( $post_object ): 
											$post = $post_object;
											setup_postdata( $post ); 
										?>
											<a href="<?php the_permalink(); ?>" class="program-title-link">
												<?php the_title(); ?>
											</a>
											<p>
												<?php the_excerpt(); ?>
											</p>
											<a href="<?php the_permalink(); ?>">
												Learn more
											</a>
										    <?php wp_reset_postdata(); ?>
										<?php endif; ?>

										<hr class="dark">
										<p>
											<?php the_field('counseling_box_2_requirements'); ?>
										</p>
									</div><!-- .inner -->
								<?php } else { ?>
									<div class="inner" style="background: transparent !important;"></div><!-- .inner -->
								<?php } ?>
							</div><!-- .program-box -->
							<div class="program-box">
								<?php if( get_field('counseling_box_3') ) { ?>
									<div class="inner">
										<?php $post_object = get_field('counseling_box_3'); ?>
										<?php if( $post_object ): 
											$post = $post_object;
											setup_postdata( $post ); 
										?>
											<a href="<?php the_permalink(); ?>" class="program-title-link">
												<?php the_title(); ?>
											</a>
											<p>
												<?php the_excerpt(); ?>
											</p>
											<a href="<?php the_permalink(); ?>">
												Learn more
											</a>
										    <?php wp_reset_postdata(); ?>
										<?php endif; ?>

										<hr class="dark">
										<p>
											<?php the_field('counseling_box_3_requirements'); ?>
										</p>
									</div><!-- .inner -->
								<?php } else { ?>
									<div class="inner" style="background: transparent !important;"></div><!-- .inner -->
								<?php } ?>
							</div><!-- .program-box -->
							<div class="program-box">
								<?php if( get_field('counseling_box_4') ) { ?>
									<div class="inner last">
										<?php $post_object = get_field('counseling_box_4'); ?>
										<?php if( $post_object ): 
											$post = $post_object;
											setup_postdata( $post ); 
										?>
											<a href="<?php the_permalink(); ?>" class="program-title-link">
												<?php the_title(); ?>
											</a>
											<p>
												<?php the_excerpt(); ?>
											</p>
											<a href="<?php the_permalink(); ?>">
												Learn more
											</a>
										    <?php wp_reset_postdata(); ?>
										<?php endif; ?>

										<hr class="dark">
										<p>
											<?php the_field('counseling_box_4_requirements'); ?>
										</p>
									</div><!-- .inner -->
								<?php } else { ?>
									<div class="inner last" style="background: transparent !important;"></div><!-- .inner -->
								<?php } ?>
							</div><!-- .program-box -->
						</div><!-- .left -->
						<div class="right">
							<div class="program-head">
								<h3 class="goudy">
									<em><?php the_field('coaching_specialties_title'); ?></em>
								</h3>
							</div><!-- .programs-head -->
							<div class="program-box">
								<?php if( get_field('coaching_box_1') ) { ?>
									<div class="inner first">
										<?php $post_object = get_field('coaching_box_1'); ?>
											<?php if( $post_object ): 
												$post = $post_object;
												setup_postdata( $post ); 
											?>
												<a href="<?php the_permalink(); ?>" class="program-title-link">
													<?php the_title(); ?>
												</a>
												<p>
													<?php the_excerpt(); ?>
												</p>
												<a href="<?php the_permalink(); ?>">
													Learn more
												</a>
											    <?php wp_reset_postdata(); ?>
											<?php endif; ?>
										<hr class="dark">
										<p>
											<?php the_field('coaching_box_1_requirements'); ?>
										</p>
									</div><!-- .inner -->
								<?php } else { ?>
									<div class="inner first" style="background: transparent !important;"></div><!-- .inner -->
								<?php } ?>
							</div><!-- .program-box -->
							<div class="program-box">
								<?php if( get_field('coaching_box_2') ) { ?>
									<div class="inner last">
										<?php $post_object = get_field('coaching_box_2'); ?>
											<?php if( $post_object ): 
												$post = $post_object;
												setup_postdata( $post ); 
											?>
												<a href="<?php the_permalink(); ?>" class="program-title-link">
													<?php the_title(); ?>
												</a>
												<p>
													<?php the_excerpt(); ?>
												</p>
												<a href="<?php the_permalink(); ?>">
													Learn more
												</a>
											    <?php wp_reset_postdata(); ?>
											<?php endif; ?>
										<hr class="dark">
										<p>
											<?php the_field('coaching_box_2_requirements'); ?>
										</p>
									</div><!-- .inner -->
								<?php } else { ?>
									<div class="inner last" style="background: transparent !important;"></div><!-- .inner -->
								<?php } ?>
							</div><!-- .program-box -->
						</div><!-- .col-md-4 -->
						<div class="clear"></div><!-- .clear -->
					</div><!-- .programs-wrap -->
				</div><!-- .container  -->
			</div><!-- .programs-wrap -->
			<div class="container">
				<div class="maintaining-certification-wrap">
					<div class="container">
						<h2 class="section-head goudy">
							<em><?php the_field('maintain_certification_title'); ?></em>
						</h2>
						<hr class="dark">
						<div class="content">
							<?php the_field('maintaining_certification'); ?>
							<div class="lists-wrap">
								<div class="col-md-6">
									<?php the_field('left_box'); ?>
								</div><!-- .col-md-6 -->
								<div class="col-md-6">
									<?php the_field('right_box'); ?>
								</div><!-- .col-md-6 -->
								<div class="clear"></div><!-- .clear -->
								<div class="cta">
									<?php the_field('call_to_action_text'); ?>
									<div class="buttons-wrap">
										<div class="col-md-5">
											<a href="<?php the_field('left_button_link'); ?>" target="_blank" class="cert-cta-btn goudy">
												<?php the_field('left_button_text'); ?>
											</a>
										</div><!-- .col-md-5 -->
										<div class="col-md-7">
											<a href="<?php the_field('right_button_link'); ?>" target="_blank" class="cert-cta-btn goudy">
												<?php the_field('right_button_text'); ?>
											</a>
										</div><!-- .col-md-7 -->
										<div class="clear"></div><!-- .clear -->
									</div><!-- .buttons-wrap -->
								</div><!-- .cta -->
							</div><!-- lists-wrap -->
						</div><!-- .content -->
					</div><!-- .container -->
				</div><!-- .maintaining-certification-wrap -->
			</div><!-- .container -->
		</article><!-- .entry -->
	<?php endwhile; ?>
	<?php else : ?>
	<?php get_template_part('blocks/content/loop', 'nocontent'); ?>
<?php endif; ?>
<?php get_footer(); ?>