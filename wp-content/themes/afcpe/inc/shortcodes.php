<?php
	// Example -- [afcpe_menu]
	function afcpe_menu_function( $attr ) {
		ob_start(); ?>
			<?php $args = array( 'post_type' => 'afcpe_menu', 'posts_per_page' => -1 ); ?>
			<?php $loop = new WP_Query( $args ); ?>
			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<?php the_title(); ?>
				<br />
				<?php the_content(); ?>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		<?php return ob_get_clean();
	}
	add_shortcode( 'afcpe_menu', 'afcpe_menu_function' );
?>