<?php
	// Register Career Path Post Type
	function career_path_type() {

		$labels = array(
			'name'                  => _x( 'Career Paths', 'Post Type General Name', 'afcpe' ),
			'singular_name'         => _x( 'Career Path', 'Post Type Singular Name', 'afcpe' ),
			'menu_name'             => __( 'Career Paths', 'afcpe' ),
			'name_admin_bar'        => __( 'Career Paths', 'afcpe' ),
			'archives'              => __( 'Career Path Archives', 'afcpe' ),
			'parent_item_colon'     => __( 'Parent Path:', 'afcpe' ),
			'all_items'             => __( 'All Career Paths', 'afcpe' ),
			'add_new_item'          => __( 'Add New Career Path', 'afcpe' ),
			'add_new'               => __( 'Add New', 'afcpe' ),
			'new_item'              => __( 'New Career Path', 'afcpe' ),
			'edit_item'             => __( 'Edit Career Path', 'afcpe' ),
			'update_item'           => __( 'Update Career Path', 'afcpe' ),
			'view_item'             => __( 'View Career Path', 'afcpe' ),
			'search_items'          => __( 'Search Career Path', 'afcpe' ),
			'not_found'             => __( 'Not found', 'afcpe' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'afcpe' ),
			'featured_image'        => __( 'Featured Image', 'afcpe' ),
			'set_featured_image'    => __( 'Set featured image', 'afcpe' ),
			'remove_featured_image' => __( 'Remove featured image', 'afcpe' ),
			'use_featured_image'    => __( 'Use as featured image', 'afcpe' ),
			'insert_into_item'      => __( 'Insert into Career Path', 'afcpe' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Career Path', 'afcpe' ),
			'items_list'            => __( 'Career paths list', 'afcpe' ),
			'items_list_navigation' => __( 'Career paths list navigation', 'afcpe' ),
			'filter_items_list'     => __( 'Filter career paths list', 'afcpe' ),
		);
		$args = array(
			'label'                 => __( 'Career Path', 'afcpe' ),
			'description'           => __( 'Career Paths', 'afcpe' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'page-attributes', ),
			'taxonomies'            => array( 'career_path_category' ),
			'hierarchical'          => true,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-businessman',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => true,		
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
		);
		register_post_type( 'career_path', $args );

	}
	add_action( 'init', 'career_path_type', 0 );

	// Register Career Path Category Taxonomy
	function career_path_category_tax() {

		$labels = array(
			'name'                       => _x( 'Categories', 'Taxonomy General Name', 'afcpe' ),
			'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'afcpe' ),
			'menu_name'                  => __( 'Categories', 'afcpe' ),
			'all_items'                  => __( 'All Categories', 'afcpe' ),
			'parent_item'                => __( 'Parent Category', 'afcpe' ),
			'parent_item_colon'          => __( 'Parent Category:', 'afcpe' ),
			'new_item_name'              => __( 'New Category Name', 'afcpe' ),
			'add_new_item'               => __( 'Add New Category', 'afcpe' ),
			'edit_item'                  => __( 'Edit Category', 'afcpe' ),
			'update_item'                => __( 'Update Category', 'afcpe' ),
			'view_item'                  => __( 'View Category', 'afcpe' ),
			'separate_items_with_commas' => __( 'Separate categories with commas', 'afcpe' ),
			'add_or_remove_items'        => __( 'Add or remove categories', 'afcpe' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'afcpe' ),
			'popular_items'              => __( 'Popular categories', 'afcpe' ),
			'search_items'               => __( 'Search Categories', 'afcpe' ),
			'not_found'                  => __( 'Not Found', 'afcpe' ),
			'no_terms'                   => __( 'No Categories', 'afcpe' ),
			'items_list'                 => __( 'Categories list', 'afcpe' ),
			'items_list_navigation'      => __( 'Categories list navigation', 'afcpe' ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
		);
		register_taxonomy( 'career_path_category', array( 'career_path' ), $args );

	}
	add_action( 'init', 'career_path_category_tax', 0 );
?>