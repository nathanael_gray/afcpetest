<?php
	// Register Menu Post Type
	function afcp_menu_type() {

		$labels = array(
			'name'                  => _x( 'Menus', 'Post Type General Name', 'afcpe' ),
			'singular_name'         => _x( 'Menu', 'Post Type Singular Name', 'afcpe' ),
			'menu_name'             => __( 'Menus', 'afcpe' ),
			'name_admin_bar'        => __( 'Menus', 'afcpe' ),
			'archives'              => __( 'Menu Archives', 'afcpe' ),
			'parent_item_colon'     => __( 'Parent Menu:', 'afcpe' ),
			'all_items'             => __( 'All Menus', 'afcpe' ),
			'add_new_item'          => __( 'Add New Menu', 'afcpe' ),
			'add_new'               => __( 'Add New', 'afcpe' ),
			'new_item'              => __( 'New Menu', 'afcpe' ),
			'edit_item'             => __( 'Edit Menu', 'afcpe' ),
			'update_item'           => __( 'Update Menu', 'afcpe' ),
			'view_item'             => __( 'View Menu', 'afcpe' ),
			'search_items'          => __( 'Search Menu', 'afcpe' ),
			'not_found'             => __( 'Not found', 'afcpe' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'afcpe' ),
			'featured_image'        => __( 'Featured Image', 'afcpe' ),
			'set_featured_image'    => __( 'Set featured image', 'afcpe' ),
			'remove_featured_image' => __( 'Remove featured image', 'afcpe' ),
			'use_featured_image'    => __( 'Use as featured image', 'afcpe' ),
			'insert_into_item'      => __( 'Insert into menu', 'afcpe' ),
			'uploaded_to_this_item' => __( 'Uploaded to this menu', 'afcpe' ),
			'items_list'            => __( 'Menus list', 'afcpe' ),
			'items_list_navigation' => __( 'Menus list navigation', 'afcpe' ),
			'filter_items_list'     => __( 'Filter menus list', 'afcpe' ),
		);
		$args = array(
			'label'                 => __( 'Menu', 'afcpe' ),
			'description'           => __( 'AFCPE Menu', 'afcpe' ),
			'labels'                => $labels,
			'supports'              => array( 'title' ),
			'taxonomies'            => array(),
			'hierarchical'          => true,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-menu',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => false,		
			'exclude_from_search'   => true,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
		);
		register_post_type( 'afcpe_menu', $args );

	}
	add_action( 'init', 'afcp_menu_type', 0 );
?>