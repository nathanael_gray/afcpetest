<?php

	// Get our plugins
	require_once('inc/required-plugins.php');

	// Get our shortcodes
	require_once('inc/shortcodes.php');

	// Get our custom post types
	require_once('inc/post-types.php');

	add_action('init', 'init_remove_support',100);
	function init_remove_support(){
		$post_type = 'afcpe_menu';
		remove_post_type_support( $post_type, 'editor');
	}

	// Filter the title meta tag
	function wpdocs_filter_wp_title( $title, $sep ) {
		global $paged, $page;

		if ( is_feed() )
			return $title;

		// Add the site name.
		$title .= get_bloginfo( 'name' );

		// Add the site description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			$title = "$title $sep $site_description";

		// Add a page number if necessary.
		if ( $paged >= 2 || $page >= 2 )
			$title = "$title $sep " . sprintf( __( 'Page %s', 'afcpe' ), max( $paged, $page ) );
		return $title;
	}
	add_filter( 'wp_title', 'wpdocs_filter_wp_title', 10, 2 );


	// Make the breadcrumbs
	function the_breadcrumb() {
		if (!is_home()) {
			echo '<a href="';
			echo esc_url( home_url() );
			echo '">';
			echo 'Home';
			echo "</a> | ";
			if (is_category() || is_single()) {
				the_category('<span> <i class="fa fa-angle-double-right"></i> ');
				if (is_single()) {
					echo " | ";
					the_title();
				}
			} elseif (is_page()) {
				echo the_title();
			}
		}
	}

	// Register Theme Features
	function afcpe_theme_features()  {

		// Add theme support for Automatic Feed Links
		add_theme_support( 'automatic-feed-links' );

		// Add theme support for Featured Images
		add_theme_support( 'post-thumbnails' );

		 // Set custom thumbnail dimensions
		set_post_thumbnail_size( 150, 150, true );

		// Add theme support for HTML5 Semantic Markup
		add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );

		// Add theme support for document Title tag
		add_theme_support( 'title-tag' );

		// Add theme support for Translation
		load_theme_textdomain( 'afcpe', get_template_directory() . '/language' );
	}
	add_action( 'after_setup_theme', 'afcpe_theme_features' );

	// Enable excerpt for pages to fix visual composer search results
	add_post_type_support('page', 'excerpt');

	// Register our menus
	function afcpe_menus() {

		$locations = array(
			'primary' => __( 'Primary Menu', 'afcpe' ),
			'header-inner' => __( 'Inner Header Menu', 'afcpe' ),
			'header-login' => __( 'Login Header Menu', 'afcpe' ),
			'footer' => __( 'Footer Menu', 'afcpe' ),
		);
		register_nav_menus( $locations );

	}
	add_action( 'init', 'afcpe_menus' );

	// Remove ul from wp_nav_menu for easier noob control
	function remove_ul ( $menu ){
		return preg_replace( array( '#^<ul[^>]*>#', '#</ul>$#' ), '', $menu );
	}
	add_filter( 'wp_nav_menu', 'remove_ul' );

	// Add "active" class to active menu item
	add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
	function special_nav_class($classes, $item){
		if( in_array('current-menu-item', $classes) ){
			$classes[] = 'active ';
		}
		return $classes;
	}

	// Add class to first and last li in nav menus
	function add_first_and_last($output) {
		$output = preg_replace('/class="menu-item/', 'class="first menu-item', $output, 1);
		$output = substr_replace($output, 'class="last menu-item', strripos($output, 'class="menu-item'), strlen('class="menu-item'));
		return $output;
	}
	add_filter('wp_nav_menu', 'add_first_and_last');	

	$pluginList = get_option( 'active_plugins' );
	$plugin = 'advanced-custom-fields-pro/acf.php'; 
	// AFCPE Options
	if ( in_array( $plugin , $pluginList ) ) {
		if( function_exists('acf_add_options_page') ) {
			acf_add_options_page(array(
				'page_title' 	=> 'AFCPE',
				'menu_title' 	=> 'AFCPE',
				'icon_url'		=> get_template_directory_uri() . '/img/afcpe-small.png',
				'menu_slug' 	=> 'afcpe-settings',
				'capability' 	=> 'edit_posts',
				'position'		=> '',
				'redirect' 		=> false
			));		
//			acf_add_options_sub_page(array(
//				'page_title' 	=> 'Header Options',
//				'menu_title'	=> 'Header',
//				'parent_slug'	=> 'afcpe-settings',
//			));
			acf_add_options_sub_page(array(
				'page_title' 	=> 'Events Page Settings',
				'menu_title'	=> 'Events',
				'parent_slug'	=> 'afcpe-settings',
			));
			acf_add_options_sub_page(array(
				'page_title' 	=> 'Footer Options',
				'menu_title'	=> 'Footer',
				'parent_slug'	=> 'afcpe-settings',
			));
		}

		// ACF styles
		function generate_options_css() {
			$ss_dir = get_stylesheet_directory();
			ob_start(); // Capture all output into buffer
			require($ss_dir . '/inc/acf-style.php'); // Grab the acf-style.php file
			$css = ob_get_clean(); // Store output in a variable, then flush the buffer
			file_put_contents($ss_dir . '/css/acf-style.css', $css, LOCK_EX); // Save it as a css file
		}
		add_action( 'acf/save_post', 'generate_options_css' ); //Parse the output and write the CSS file on post save
	}

	// Remove TinyMCE from custom page templates
	add_action( 'init', 'remove_editor_init' );

	function remove_editor_init() {
		// If not in the admin, return.
		if ( ! is_admin() ) {
			return;
		}

		// Get the post ID on edit post with filter_input super global inspection.
		$current_post_id = filter_input( INPUT_GET, 'post', FILTER_SANITIZE_NUMBER_INT );
		// Get the post ID on update post with filter_input super global inspection.
		$update_post_id = filter_input( INPUT_POST, 'post_ID', FILTER_SANITIZE_NUMBER_INT );

		// Check to see if the post ID is set, else return.
		if ( isset( $current_post_id ) ) {
			$post_id = absint( $current_post_id );
		} else if ( isset( $update_post_id ) ) {
			$post_id = absint( $update_post_id );
		} else {
			return;
		}

		// Don't do anything unless there is a post_id.
		if ( isset( $post_id ) ) {
			// Get the template of the current post.
			$template_file = get_post_meta( $post_id, '_wp_page_template', true );

			// Example of removing page editor for front-page.php template.
			if (  'front-page.php' === $template_file ) {
				remove_post_type_support( 'page', 'editor' );
				// Other features can also be removed in addition to the editor. See: https://codex.wordpress.org/Function_Reference/remove_post_type_support.
			}

			if (  'certification-center.php' === $template_file ) {
				remove_post_type_support( 'page', 'editor' );
				// Other features can also be removed in addition to the editor. See: https://codex.wordpress.org/Function_Reference/remove_post_type_support.
			}

			if (  'about-afcpe.php' === $template_file ) {
				remove_post_type_support( 'page', 'editor' );
				// Other features can also be removed in addition to the editor. See: https://codex.wordpress.org/Function_Reference/remove_post_type_support.
			}

			if (  'membership.php' === $template_file ) {
				remove_post_type_support( 'page', 'editor' );
				// Other features can also be removed in addition to the editor. See: https://codex.wordpress.org/Function_Reference/remove_post_type_support.
			}

			if (  'career-center.php' === $template_file ) {
				remove_post_type_support( 'page', 'editor' );
				// Other features can also be removed in addition to the editor. See: https://codex.wordpress.org/Function_Reference/remove_post_type_support.
			}

			if (  'page-builder.php' === $template_file ) {
				remove_post_type_support( 'page', 'editor' );
				// Other features can also be removed in addition to the editor. See: https://codex.wordpress.org/Function_Reference/remove_post_type_support.
			}

			if (  'resource-center-child.php' === $template_file ) {
				remove_post_type_support( 'page', 'editor' );
				// Other features can also be removed in addition to the editor. See: https://codex.wordpress.org/Function_Reference/remove_post_type_support.
			}

			if (  'maintaining-certification.php' === $template_file ) {
				remove_post_type_support( 'page', 'editor' );
				// Other features can also be removed in addition to the editor. See: https://codex.wordpress.org/Function_Reference/remove_post_type_support.
			}
		}
	}

	/*
	 * Alters event's archive titles
	 */
	function tribe_alter_event_archive_titles ( $original_recipe_title, $depth ) {

		// Modify the titles here
		// Some of these include %1$s and %2$s, these will be replaced with relevant dates
		$title_upcoming =   'Upcoming Programs'; // List View: Upcoming events
		$title_past =       'Past Programs'; // List view: Past events
		$title_range =      'Programs for %1$s - %2$s'; // List view: range of dates being viewed
		$title_month =      'Programs for %1$s'; // Month View, %1$s = the name of the month
		$title_day =        'Programs for %1$s'; // Day View, %1$s = the day
		$title_all =        'All programs for %s'; // showing all recurrences of an event, %s = event title
		$title_week =       'Programs for week of %s'; // Week view

		// Don't modify anything below this unless you know what it does
		global $wp_query;
		$tribe_ecp = Tribe__Events__Main::instance();
		$date_format = apply_filters( 'tribe_events_pro_page_title_date_format', tribe_get_date_format( true ) );

		// Default Title
		$title = $title_upcoming;

		// If there's a date selected in the tribe bar, show the date range of the currently showing events
		if ( isset( $_REQUEST['tribe-bar-date'] ) && $wp_query->have_posts() ) {

			if ( $wp_query->get( 'paged' ) > 1 ) {
				// if we're on page 1, show the selected tribe-bar-date as the first date in the range
				$first_event_date = tribe_get_start_date( $wp_query->posts[0], false );
			} else {
				//otherwise show the start date of the first event in the results
				$first_event_date = tribe_format_date( $_REQUEST['tribe-bar-date'], false );
			}

			$last_event_date = tribe_get_end_date( $wp_query->posts[ count( $wp_query->posts ) - 1 ], false );
			$title = sprintf( $title_range, $first_event_date, $last_event_date );
		} elseif ( tribe_is_past() ) {
			$title = $title_past;
		}

		// Month view title
		if ( tribe_is_month() ) {
			$title = sprintf(
				$title_month,
				date_i18n( tribe_get_option( 'monthAndYearFormat', 'F Y' ), strtotime( tribe_get_month_view_date() ) )
			);
		}

		// Day view title
		if ( tribe_is_day() ) {
			$title = sprintf(
				$title_day,
				date_i18n( tribe_get_date_format( true ), strtotime( $wp_query->get( 'start_date' ) ) )
			);
		}

		// All recurrences of an event
		if ( function_exists('tribe_is_showing_all') && tribe_is_showing_all() ) {
			$title = sprintf( $title_all, get_the_title() );
		}

		// Week view title
		if ( function_exists('tribe_is_week') && tribe_is_week() ) {
			$title = sprintf(
				$title_week,
				date_i18n( $date_format, strtotime( tribe_get_first_week_day( $wp_query->get( 'start_date' ) ) ) )
			);
		}

		if ( is_tax( $tribe_ecp->get_event_taxonomy() ) && $depth ) {
			$cat = get_queried_object();
			$title = '<a href="' . esc_url( tribe_get_events_link() ) . '">' . $title . '</a>';
			$title .= ' &#8250; ' . $cat->name;
		}

		return $title;
	}
	add_filter( 'tribe_get_events_title', 'tribe_alter_event_archive_titles', 11, 2 );











	// Register and enqueue our front-end scripts and styles - top-to-bottom higher in the list results in a lower priority for styles
	function register_styles_n_scripts() {
		// Keep scrits and style from loading in the WordPress Derpboard
		if ( ! is_admin() ) {
			// Stylesheets
			wp_register_style('fontawesome', get_template_directory_uri() . '/css/font-awesome.min.css', null, null, 'all' );
			wp_enqueue_style( 'fontawesome' );

			wp_register_style('bootstrapcss', get_template_directory_uri() . '/css/bootstrap.min.css', null, null, 'all' );
			wp_enqueue_style( 'bootstrapcss' );

			wp_register_style('goudy-font', '//fonts.googleapis.com/css?family=Goudy+Bookletter+1911', null, null, 'all' );
			wp_enqueue_style( 'goudy-font' );

//			wp_register_style('jq-smart-menu-base', get_template_directory_uri() . '/css/sm-simple.css', null, null, 'all' );
//			wp_enqueue_style( 'jq-smart-menu-base' );

//			wp_register_style('sm-simple', get_template_directory_uri() . '/css/sm-core-css.css', null, null, 'all' );
//			wp_enqueue_style( 'sm-simple' );

			// Enable ACF to write custom styles from the WordPress Derpboard
			$pluginList = get_option( 'active_plugins' );
			$plugin = 'advanced-custom-fields-pro/acf.php'; 
			if ( in_array( $plugin , $pluginList ) ) {
				wp_register_style('acf-style', get_template_directory_uri() . '/css/acf-style.css', null, null, 'all' );
				wp_enqueue_style( 'acf-style' );
			}

			wp_register_style('afcpe', get_template_directory_uri() . '/css/style.css', null, null, 'all' );
			wp_enqueue_style( 'afcpe' );

			// Scripts
			wp_register_script( 'modernizr', get_template_directory_uri() . '/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js', null, null, false );
			wp_enqueue_script( 'modernizr' );

			wp_register_script( 'bootstrapjs', get_template_directory_uri() . '/js/vendor/bootstrap.min.js', array( 'jquery' ), null, false );
			wp_enqueue_script( 'bootstrapjs' );

			wp_register_script( 'match-height', get_template_directory_uri() . '/js/vendor/jquery-matchHeight.js', array( 'jquery' ), null, false );
			wp_enqueue_script( 'match-height' );

//			wp_register_script( 'smart-menus', get_template_directory_uri() . '/js/vendor/jquery.smartmenus.js', array( 'jquery' ), null, false );
//			wp_enqueue_script( 'smart-menus' );

			wp_register_script( 'script', get_template_directory_uri() . '/js/script.js', array( 'jquery' ), null, false );
			wp_enqueue_script( 'script' );
		}
	}
	add_action( 'wp_enqueue_scripts', 'register_styles_n_scripts' );
?>