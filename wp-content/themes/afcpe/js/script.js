jQuery( document ).ready(function( $ ) {

	// primary nav toggle
	$('.primary-nav-toggle').on('click', function() {
		$('.primary-nav-wrap').slideToggle('fast');
		$('.head-lower').slideToggle('fast');
		$('nav.pr').slideToggle('fast');
		$('a.afcpe-nav', this).toggleClass('nav-bars nav-close');
		$('.overlay').toggle();
	});

	$('.menu-child').parent().addClass('has-children');
	$( '.has-children' ).prepend( '<span class="toggle-down"></span>' );

	$('span.toggle-down').on('click', function() {
		$(this).toggleClass('toggle-down toggle-up');
		$(this).siblings().next('.menu-child').slideToggle('fast');
	});

	// back to top
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 700,
		//grab the "back to top" link
		$back_to_top = $('.back-to-top');



	//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		 	}, scroll_top_duration
		);
	});

	// Match Height
	$(function() {
		$('.program-box .inner').matchHeight();
		$('h3.box-title').matchHeight();
		$('.event .gray').matchHeight();
		$('.equal-height').matchHeight();
	});

	// Section toggles
	$('.toggle-container').on('click', function() {
		$('.section-toggle', this).toggleClass('section-toggle-down section-toggle-up');
		$(this).next('.toggled-container').slideToggle('fast');
	});
});