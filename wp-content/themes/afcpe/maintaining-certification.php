<?php
/*
Template Name: Maintaining Certification
*/
get_header(); ?>
<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<article id="post-<?php the_ID(); ?>" class="entry post-entry">
			<div class="section white-bg">
				<div class="container">
					<h2 class="section-head goudy">
						<em>White Section</em>
					</h2>
					<hr class="dark">
				</div><!-- .container -->
			</div><!-- .section .white-bg -->










			<div id="annual_certification_fees" class="section gray-bg">
				<div class="container">
					<h2 class="section-head goudy">
						<em>Annual Certification Fees</em>
					</h2>
					<hr class="dark">

<table class="footable">
	<thead>
		<tr>
			<th>CEU Type*</th>
			<th data-hide="phone,tablet">Activity*</th>
			<th data-hide="phone">CEU Credits Allowed</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><strong>AFCPE® Annual Symposium and Pre-Conference</strong></td>
			<td>
				<div class="inner">
					<p>Attendee</p>
				</div><!-- .inner -->
				<div class="inner">
					<p>Presenter</p>
				</div><!-- .inner -->
				<div class="inner">
					<p>Live Stream/Recordings</p>
				</div><!-- .inner -->
			</td>
			<td>
				<div class="inner">
					<p>15 CEUs/year</p>
				</div><!-- .inner -->
				<div class="inner">
					<p>5 CEUs/year</p>
				</div><!-- .inner -->
				<div class="inner">
					<p>Based on available programming</p>
				</div><!-- .inner -->
			</td>
		</tr>

		<tr>
			<td>Non-AFCPE® conferences or local professional financial training</td>
			<td>phone</td>
			<td>email</td>
		</tr>

		<tr>
			<td>On-the-Job Traininge</td>
			<td>phone</td>
			<td>email</td>
		</tr>

		<tr>
			<td>Webinars</td>
			<td>phone</td>
			<td>email</td>
		</tr>

		<tr>
			<td>Research & Publishing</td>
			<td>phone</td>
			<td>email</td>
		</tr>

		<tr>
			<td>AFCPE® Member Opportunities</td>
			<td>phone</td>
			<td>email</td>
		</tr>

		<tr>
			<td>Education</td>
			<td>phone</td>
			<td>email</td>
		</tr>

		<tr>
			<td>Instruction</td>
			<td>phone</td>
			<td>email</td>
		</tr>

		<tr>
			<td>Ethics</td>
			<td>phone</td>
			<td>email</td>
		</tr>

		<tr>
			<td>Professional Designation</td>
			<td>phone</td>
			<td>email</td>
		</tr>

	</tbody>
</table>				
				</div><!-- .container -->
			</div><!-- .section .gray-bg -->

















		</article><!-- .entry -->
	<?php endwhile; ?>
	<?php else : ?>
	<?php get_template_part('blocks/content/loop', 'nocontent'); ?>
<?php endif; ?>
<?php get_footer(); ?>