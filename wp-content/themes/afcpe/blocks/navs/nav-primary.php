<?php if ( has_nav_menu( 'primary' ) ) { ?>
	<nav id="primary-nav" class="clear">
		<div class="container">
			<ul id="primary-menu" class="sm sm-simple">
				<?php
					$defaults = array(
						'theme_location'  => 'primary',
						'menu'            => 'primary',
						'container'       => false,
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'depth'           => 0,
						'walker'          => ''
					);
					wp_nav_menu( $defaults );
				?>
			</ul>
		</div><!-- .container -->
	</nav>
<?php } ?>