<?php if ( has_nav_menu( 'header-login' ) ) { ?>
	<nav id="header-login">
		<ul>
			<?php
				$defaults = array(
					'theme_location'  => 'header-login',
					'menu'            => 'header-login',
					'container'       => false,
					'echo'            => true,
					'fallback_cb'     => 'wp_page_menu',
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'depth'           => 0,
					'walker'          => ''
				);
				wp_nav_menu( $defaults );
			?>
		</ul>
	</nav>		
<?php } ?>