<?php if ( has_nav_menu( 'footer' ) ) { ?>
	<nav id="footer-nav">
		<ul id="footer-menu">
			<?php
				$defaults = array(
					'theme_location'  => 'footer',
					'menu'            => 'footer',
					'container'       => false,
					'echo'            => true,
					'fallback_cb'     => 'wp_page_menu',
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'depth'           => 0,
					'walker'          => ''
				);
				wp_nav_menu( $defaults );
			?>
		</ul>
	</nav>
<?php } ?>