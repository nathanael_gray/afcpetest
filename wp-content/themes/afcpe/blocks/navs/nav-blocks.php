<?php if(get_sub_field('link_type') == "Page / Post / Custom Post Type") { ?>
	<?php if( get_sub_field('use_custom_navigation_label') ) { ?>
		<?php if( get_sub_field('open_in_new_tab') ) { ?>
			<?php $postid = get_sub_field('item') ?>
			<a href="<?php echo get_permalink( $postid ); ?>" target="_blank">
				<?php the_sub_field('navigation_label'); ?>
			</a>

		<?php } else { ?>
			<?php $postid = get_sub_field('item') ?>						
			<a href="<?php echo get_permalink( $postid ); ?>">
				<?php the_sub_field('navigation_label'); ?>
			</a>
		<?php } ?>
	<?php } else { ?>
		<?php if( get_sub_field('open_in_new_tab') ) { ?>
			<?php $postid = get_sub_field('item') ?>
			<a href="<?php echo get_permalink( $postid ); ?>" target="_blank">
				<?php echo get_the_title( $postid ); ?>
			</a>

		<?php } else { ?>
			<?php $postid = get_sub_field('item') ?>
			<a href="<?php echo get_permalink( $postid ); ?>">
				<?php echo get_the_title( $postid ); ?>
			</a>
		<?php } ?>
	<?php } ?>
<?php } ?>
<?php if(get_sub_field('link_type') == "Custom") { ?>
	<?php if( get_sub_field('open_in_new_tab') ) { ?>
		<?php $post_object = get_sub_field('item'); ?>
		<a href="<?php the_sub_field('url'); ?>" target="_blank">
			<?php the_sub_field('navigation_label'); ?>
		</a>
	<?php } else { ?>
		<?php $post_object = get_sub_field('item'); ?>
		<a href="<?php the_sub_field('url'); ?>">
			<?php the_sub_field('navigation_label'); ?>
		</a>
	<?php } ?>
<?php } ?>