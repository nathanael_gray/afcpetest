<?php if ( has_nav_menu( 'header-inner' ) ) { ?>
	<nav id="header-inner">
		<ul>
			<?php
				$defaults = array(
					'theme_location'  => 'header-inner',
					'menu'            => 'header-inner',
					'container'       => false,
					'echo'            => true,
					'fallback_cb'     => 'wp_page_menu',
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'depth'           => 0,
					'walker'          => ''
				);
				wp_nav_menu( $defaults );
			?>
		</ul>
	</nav>		
<?php } ?>