
<?php
	$args=array(
		'post_type' => 'afcpe_menu',
		'post_status' => 'publish',
		'post_id'	=> get_field('primary_menu', 'options'),
		'posts_per_page' => 1
	);
?>
<?php $my_query = null; ?>
<?php $my_query = new WP_Query($args); ?>


<?php if( $my_query->have_posts() ) { ?>
	<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>

















<?php if( have_rows('menu_items') ): ?>
	<div class="primary-nav-wrap">
		<div class="container">
			<nav class="primary-nav">
				<ul class="primary-menu">
					<?php while( have_rows('menu_items') ): the_row(); ?>

						<?php if( get_sub_field('mobile_only') ) { ?>
							<li class="mobile-only">
						<?php } else { ?>
							<li>
						<?php } ?>						

							<?php get_template_part('blocks/navs/nav', 'parents'); ?>

							<?php get_template_part('blocks/navs/nav', 'columns'); ?>

						</li>
					<?php endwhile; ?>
				</ul>
			</nav>
		</div><!-- .container -->
	</div><!-- .primary-nav-wrap -->
<?php endif; ?>












	<?php endwhile; } ?>


<?php wp_reset_query(); ?>