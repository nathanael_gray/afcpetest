<?php if( have_rows('columns') ): ?>
	<div class="menu-child">
		<?php while ( have_rows('columns') ) : the_row(); ?>

			<?php if( get_row_layout() == 'one_column' ): ?>
				<div class="container">
					<div class="col-md-12">
						
						<?php if( get_sub_field('col_1_1_head') ) { ?>
							<div class="column-head">
								<?php the_sub_field('col_1_1_head'); ?>
							</div><!-- .column-head -->
						<?php } ?>

						<?php if( get_sub_field('col_1_1_sub_head') ) { ?>
							<h4 class="menu-item-sub-head">
								<?php the_sub_field('col_1_1_sub_head'); ?>
							</h4>
						<?php } ?>

						<?php if( have_rows('col_1_1') ): ?>
							<ul>
								<?php while ( have_rows('col_1_1') ) : the_row(); ?>
									<li>
										<?php get_template_part('blocks/navs/nav', 'blocks'); ?>
									</li>
								<?php endwhile; ?>
							</ul>
							<?php else : ?>

						<?php endif; ?>

					</div><!-- .col-md-12 -->
					<div class="clear"></div><!-- .clear -->
				</div><!-- .container -->
			<?php endif; ?>

			<?php if( get_row_layout() == 'two_columns' ): ?>
				<div class="container">
					<div class="col-md-6">
						<?php if( get_sub_field('col_2_1_head') ) { ?>
							<div class="column-head">
								<?php the_sub_field('col_2_1_head'); ?>
							</div><!-- .column-head -->
						<?php } ?>

						<?php if( get_sub_field('col_2_1_sub_head') ) { ?>
							<h4 class="menu-item-sub-head">
								<?php the_sub_field('col_2_1_sub_head'); ?>
							</h4>
						<?php } ?>

						<?php if( have_rows('col_2_1') ): ?>
							<ul>
								<?php while ( have_rows('col_2_1') ) : the_row(); ?>
									<li>
										<?php get_template_part('blocks/navs/nav', 'blocks'); ?>
									</li>
								<?php endwhile; ?>
							</ul>
							<?php else : ?>

						<?php endif; ?>
					</div><!-- .col-md-6 -->
					<div class="col-md-6">
						<?php if( get_sub_field('col_2_2_head') ) { ?>
							<div class="column-head">
								<?php the_sub_field('col_2_2_head'); ?>
							</div><!-- .column-head -->
						<?php } ?>

						<?php if( get_sub_field('col_2_2_sub_head') ) { ?>
							<h4 class="menu-item-sub-head">
								<?php the_sub_field('col_2_2_sub_head'); ?>
							</h4>
						<?php } ?>

						<?php if( have_rows('col_2_2') ): ?>
							<ul>
								<?php while ( have_rows('col_2_2') ) : the_row(); ?>
									<li>
										<?php get_template_part('blocks/navs/nav', 'blocks'); ?>
									</li>
								<?php endwhile; ?>
							</ul>
							<?php else : ?>

						<?php endif; ?>
					</div><!-- .col-md-6 -->
					<div class="clear"></div><!-- .clear -->
				</div><!-- .container -->
			<?php endif; ?>

			<?php if( get_row_layout() == 'three_columns' ): ?>
				<div class="container">
					<div class="col-md-4">
						<?php if( get_sub_field('col_3_1_head') ) { ?>
							<div class="column-head">
								<?php the_sub_field('col_3_1_head'); ?>
							</div><!-- .column-head -->
						<?php } ?>

						<?php if( get_sub_field('col_3_1_sub_head') ) { ?>
							<h4 class="menu-item-sub-head">
								<?php the_sub_field('col_3_1_sub_head'); ?>
							</h4>
						<?php } ?>

						<?php if( have_rows('col_3_1') ): ?>
							<ul>
								<?php while ( have_rows('col_3_1') ) : the_row(); ?>
									<li>
										<?php get_template_part('blocks/navs/nav', 'blocks'); ?>
									</li>
								<?php endwhile; ?>
							</ul>
							<?php else : ?>

						<?php endif; ?>
					</div><!-- .col-md-4 -->
					<div class="col-md-4">
						<?php if( get_sub_field('col_3_2_head') ) { ?>
							<div class="column-head">
								<?php the_sub_field('col_3_2_head'); ?>
							</div><!-- .column-head -->
						<?php } ?>

						<?php if( get_sub_field('col_3_2_sub_head') ) { ?>
							<h4 class="menu-item-sub-head">
								<?php the_sub_field('col_3_2_sub_head'); ?>
							</h4>
						<?php } ?>

						<?php if( have_rows('col_3_2') ): ?>
							<ul>
								<?php while ( have_rows('col_3_2') ) : the_row(); ?>
									<li>
										<?php get_template_part('blocks/navs/nav', 'blocks'); ?>
									</li>
								<?php endwhile; ?>
							</ul>
							<?php else : ?>

						<?php endif; ?>
					</div><!-- .col-md-4 -->
					<div class="col-md-4">	
						<?php if( get_sub_field('col_3_3_head') ) { ?>
							<div class="column-head">
								<?php the_sub_field('col_3_3_head'); ?>
							</div><!-- .column-head -->
						<?php } ?>

						<?php if( get_sub_field('col_3_3_sub_head') ) { ?>
							<h4 class="menu-item-sub-head">
								<?php the_sub_field('col_3_3_sub_head'); ?>
							</h4>
						<?php } ?>

						<?php if( have_rows('col_3_3') ): ?>
							<ul>
								<?php while ( have_rows('col_3_3') ) : the_row(); ?>
									<li>
										<?php get_template_part('blocks/navs/nav', 'blocks'); ?>
									</li>
								<?php endwhile; ?>
							</ul>
							<?php else : ?>

						<?php endif; ?>
					</div><!-- .col-md-4 -->
					<div class="clear"></div><!-- .clear -->
				</div><!-- .container -->
			<?php endif; ?>

			<?php if( get_row_layout() == 'four_columns' ): ?>
				<div class="container">
					<div class="col-md-3">
						<?php if( get_sub_field('col_4_1_head') ) { ?>
							<div class="column-head">
								<?php the_sub_field('col_4_1_head'); ?>
							</div><!-- .column-head -->
						<?php } ?>

						<?php if( get_sub_field('col_4_1_sub_head') ) { ?>
							<h4 class="menu-item-sub-head">
								<?php the_sub_field('col_4_1_sub_head'); ?>
							</h4>
						<?php } ?>

						<?php if( have_rows('col_4_1') ): ?>
							<ul>
								<?php while ( have_rows('col_4_1') ) : the_row(); ?>
									<li>
										<?php get_template_part('blocks/navs/nav', 'blocks'); ?>
									</li>
								<?php endwhile; ?>
							</ul>
							<?php else : ?>

						<?php endif; ?>
					</div><!-- .col-md-3 -->
					<div class="col-md-3">
						<?php if( get_sub_field('col_4_2_head') ) { ?>
							<div class="column-head">
								<?php the_sub_field('col_4_2_head'); ?>
							</div><!-- .column-head -->
						<?php } ?>

						<?php if( get_sub_field('col_4_2_sub_head') ) { ?>
							<h4 class="menu-item-sub-head">
								<?php the_sub_field('col_4_2_sub_head'); ?>
							</h4>
						<?php } ?>

						<?php if( have_rows('col_4_2') ): ?>
							<ul>
								<?php while ( have_rows('col_4_2') ) : the_row(); ?>
									<li>
										<?php get_template_part('blocks/navs/nav', 'blocks'); ?>
									</li>
								<?php endwhile; ?>
							</ul>
							<?php else : ?>

						<?php endif; ?>
					</div><!-- .col-md-3 -->
					<div class="col-md-3">
						<?php if( get_sub_field('col_4_3_head') ) { ?>
							<div class="column-head">
								<?php the_sub_field('col_4_3_head'); ?>
							</div><!-- .column-head -->
						<?php } ?>

						<?php if( get_sub_field('col_4_3_sub_head') ) { ?>
							<h4 class="menu-item-sub-head">
								<?php the_sub_field('col_4_3_sub_head'); ?>
							</h4>
						<?php } ?>

						<?php if( have_rows('col_4_3') ): ?>
							<ul>
								<?php while ( have_rows('col_4_3') ) : the_row(); ?>
									<li>
										<?php get_template_part('blocks/navs/nav', 'blocks'); ?>
									</li>
								<?php endwhile; ?>
							</ul>
							<?php else : ?>

						<?php endif; ?>
					</div><!-- .col-md-3 -->
					<div class="col-md-3">
						<?php if( get_sub_field('col_4_4_head') ) { ?>
							<div class="column-head">
								<?php the_sub_field('col_4_4_head'); ?>
							</div><!-- .column-head -->
						<?php } ?>

						<?php if( get_sub_field('col_4_4_sub_head') ) { ?>
							<h4 class="menu-item-sub-head">
								<?php the_sub_field('col_4_4_sub_head'); ?>
							</h4>
						<?php } ?>

						<?php if( have_rows('col_4_4') ): ?>
							<ul>
								<?php while ( have_rows('col_4_4') ) : the_row(); ?>
									<li>
										<?php get_template_part('blocks/navs/nav', 'blocks'); ?>
									</li>
								<?php endwhile; ?>
							</ul>
							<?php else : ?>

						<?php endif; ?>
					</div><!-- .col-md-3 -->
					<div class="clear"></div><!-- .clear -->
				</div><!-- .container -->
			<?php endif; ?>

		<?php endwhile; ?>
	</div><!-- .menu-child -->
	<?php else : ?>
<?php endif; ?>