<div class="section blue-bg">
	<div class="container">
		<div class="inner-section full-width">
			<div class="form-wrap">
				<h2 class="subscribe-head">
					<?php the_field('heading', 'options'); ?>
				</h2>
				<h4 class="subscribe-subhead goudy">
					<?php the_field('sub_heading', 'options'); ?>
				</h4>
				<?php 
					$form_object = get_field('select_form', 'options');
					echo do_shortcode('[gravityform id="' . $form_object['id'] . '" title="false" description="false" ajax="true"]');
				?>
				<div class="after-subscribe">
					<?php the_field('subscribe_footer_content', 'options'); ?>
				</div><!-- .after-subscribe -->
			</div><!-- .form-wrap -->
		</div><!-- .inner-section .full-width -->
	</div><!-- .container -->
</div><!-- .section .white-bg -->