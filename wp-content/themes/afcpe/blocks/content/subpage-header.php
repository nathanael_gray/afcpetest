<?php if( get_field('has_grandparent') ) { ?>
	<div class="main-subpage-head-wrap">
		<div class="container">
			<div class="inner">
				<h3 class="subpage-parents goudy">
					<em>
						<?php $post_object = get_field('granparent_page'); ?>

						<?php if( $post_object ):
							$post = $post_object;
							setup_postdata( $post ); 
						?>

							<?php the_title(); ?>:
							<?php wp_reset_postdata(); ?>

						<?php $post_object = get_field('parent_page'); ?>

						<?php if( $post_object ):
							$post = $post_object;
							setup_postdata( $post ); 
						?>

							<?php the_title(); ?>
							<?php wp_reset_postdata(); ?>
						<?php endif; ?>
						<?php endif; ?>
					</em>
				</h3>
			</div><!-- .inner -->
			<h1 class="subpage-title">
				<?php the_title(); ?>
			</h1>
		</div><!-- .container -->
	</div><!-- .subpage-head-wrap -->
	<?php } elseif ( get_field('parent_page') ) { ?>
	<div class="main-subpage-head-wrap">
		<div class="container">
			<div class="inner">
				<h3 class="subpage-parents goudy">
					<em>
						<?php $post_object = get_field('parent_page'); ?>

						<?php if( $post_object ):
							$post = $post_object;
							setup_postdata( $post ); 
						?>

						<?php the_title(); ?>
						<?php wp_reset_postdata(); ?>
						<?php endif; ?>
					</em>
				</h3>
			</div><!-- .inner -->
			<h1 class="subpage-title">
				<?php the_title(); ?>
			</h1>
		</div><!-- .container -->
	</div><!-- .subpage-head-wrap -->
<?php } ?>