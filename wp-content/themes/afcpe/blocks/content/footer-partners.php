<?php $pluginList = get_option( 'active_plugins' ); ?>
<?php $plugin = 'advanced-custom-fields-pro/acf.php'; ?>
<?php if ( in_array( $plugin , $pluginList ) ) { ?>

	<?php if( have_rows('partners', 'options') ): ?>
		<div class="footer-partners-wrap">
			<div class="container">
				<h2 class="section-head goudy">
					<em>Partners</em>
				</h2>
				<hr class="dark">
				<center>
					<?php while ( have_rows('partners', 'options') ) : the_row(); ?>
						<?php if( get_sub_field('open_in_new_window') ) { ?>
							<a href="<?php the_sub_field('partner_link'); ?>" target="_blank">
						<?php } else { ?>
							<a href="<?php the_sub_field('partner_link'); ?>">
						<?php } ?>
							<img src="<?php the_sub_field('partner_logo'); ?>" class="partner-logo" />
						</a>
					<?php endwhile; ?>
				</center>
				<center>
					<a href="<?php the_field('partners_call_to_action_link', 'options'); ?>" class="partners-cta goudy" title="<?php the_field('parteners_call_to_action_text', 'options'); ?>">
						<strong><em><?php the_field('parteners_call_to_action_text', 'options'); ?></em></strong>
					</a>
				</center>
			</div><!-- .inner -->
		</div><!-- .footer-partners-wrap -->
		<?php else : ?>
	<?php endif; ?>

<?php } ?>