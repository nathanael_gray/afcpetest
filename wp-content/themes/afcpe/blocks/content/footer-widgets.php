<div class="footer-widgets">
	<div class="container">
		<?php if ( is_active_sidebar( 'footer1' )  ) { ?>
			<div class="footer-widget col-md-3">
				<?php dynamic_sidebar( 'footer1' ); ?>
			</div><!-- .footer-widget -->
		<?php } else { ?>
			<div class="footer-widget col-md-3">
			</div><!-- .footer-widget -->
		<?php } ?>

		<?php if ( is_active_sidebar( 'footer2' )  ) { ?>
			<div class="footer-widget col-md-3">
				<?php dynamic_sidebar( 'footer2' ); ?>
			</div><!-- .footer-widget -->
		<?php } else { ?>
			<div class="footer-widget col-md-3">
			</div><!-- .footer-widget -->
		<?php } ?>

		<?php if ( is_active_sidebar( 'footer3' )  ) { ?>
			<div class="footer-widget col-md-3">
				<?php dynamic_sidebar( 'footer3' ); ?>
			</div><!-- .footer-widget -->
		<?php } else { ?>
			<div class="footer-widget col-md-3">
			</div><!-- .footer-widget -->
		<?php } ?>

		<?php if ( is_active_sidebar( 'footer4' )  ) { ?>
			<div class="footer-widget col-md-3">
				<?php dynamic_sidebar( 'footer4' ); ?>
			</div><!-- .footer-widget -->
		<?php } else { ?>
			<div class="footer-widget col-md-3">
			</div><!-- .footer-widget -->
		<?php } ?>
	</div><!-- .container -->
</div><!-- .footer-widgets -->