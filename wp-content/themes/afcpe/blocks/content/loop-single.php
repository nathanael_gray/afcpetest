<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="container">
			<article id="post-<?php the_ID(); ?>" class="entry archive-entry">
				<?php get_template_part('blocks/content/content', 'breadcrumbs'); ?>
				<h3>
					<?php the_title(); ?>
				</h3>
				<small>
					By <?php the_author_posts_link(); ?>
					<br />
					Published on <a href="<?php echo get_day_link('', '', ''); ?>"><?php the_date(); ?></a>
					<br />
					Categories: <?php the_category( '&bull;' ); ?>
					<br />
					<?php the_tags( 'Tags: ', ', ','' ); ?>
					<br />
				</small>
				<div class="single-content-wrap">
					<?php the_content(); ?>
				</div><!-- .single-content-wrap -->
				<?php comments_template('/comments.php'); ?>
			</article><!-- .entry -->
		</div><!-- .container -->
	<?php endwhile; ?>
	<?php else : ?>
	<?php get_template_part('blocks/loops/loop', 'nocontent'); ?>
<?php endif; ?>