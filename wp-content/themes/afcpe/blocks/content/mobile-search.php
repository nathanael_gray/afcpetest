<div class="mobile-search-wrap">
	<div class="container">
		<form id="searchform" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
			<button type="submit">
				<span class="search-text">search</span><!-- .search-toggle -->
			</button>
			<input type="text" placeholder="Search ACFPE.com" value="" name="s" id="s" class="goudy" />
		</form>
	</div><!-- .clear -->
</div><!-- .mobile-search-wrap -->