<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="container">
			<article id="post-<?php the_ID(); ?>" class="entry archive-entry">
				<h3>
					<a href="<?php the_permalink(); ?>">
						<?php the_title(); ?>
					</a>
				</h3>
				<small>
					By <?php the_author_posts_link(); ?>
					<br />
					Published on <a href="<?php echo get_day_link('', '', ''); ?>"><?php the_date(); ?></a>
					<br />
					Categories: <?php the_category( '&bull;' ); ?>
					<br />
					<?php the_tags( 'Tags: ', ', ','' ); ?>
					<br />
				</small>
				<p>
					<?php echo get_the_excerpt(); ?> <a href="<?php the_permalink(); ?>">Learn more</a>
				</p>
			</article><!-- .entry -->
		</div><!-- .container -->
	<?php endwhile; ?>
	<?php else : ?>
	<?php get_template_part('blocks/loops/loop', 'nocontent'); ?>
<?php endif; ?>