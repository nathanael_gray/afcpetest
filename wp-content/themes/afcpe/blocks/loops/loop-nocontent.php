<div class="container" style="padding: 50px 0;">
	<center>
		<h1>
			Uh oh! Looks like there's nothing to see here.
		</h1>
		<h4>
			You might have better luck if you tried searching for content that still exists.
		</h4>
		<br />
		<br />

		<form class="form-inline" id="searchform" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
			<div class="form-group">
				<input type="text" placeholder="search" value="" name="s" id="s" class="form-control" />
			</div>
			<button type="submit" class="btn btn-default">search</button>
		</form>



	</center>
</div><!-- .container -->