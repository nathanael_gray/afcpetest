<div class="events-wrap">
	<div class="container">
		<h2 class="section-head goudy">
			<em>Events this Quarter</em>
		</h2>
		<hr class="dark">	

		<?php if( have_rows('event_boxes') ): ?>
			<?php while ( have_rows('event_boxes') ) : the_row(); ?>
				<?php if( get_row_layout() == 'event_1_col' ): ?>
					<?php $post_object = get_sub_field('event_1_1'); ?>
					<?php if( $post_object ): 
						// override $post
						$post = $post_object;
						setup_postdata( $post );
					?>
						<div class="event event-full">
							<div class="media gray">
								<div class="media-left">
									<h4 class="month">
										<?php echo tribe_get_start_date($post, false, $format = 'F' ); ?>
									</h4>
									<h1 class="day">
										<?php echo tribe_get_start_date($post, false, $format = 'j' ); ?>
									</h1>
								</div><!-- .media-left -->
								<div class="media-body">
									<h4 class="media-heading">
										<a href="<?php the_permalink(); ?>" class="event-link">
											<?php the_title(); ?>
										</a>
									</h4>
									<p class="event-summary">
										<?php echo get_the_excerpt(); ?>
										<br />
										<?php echo tribe_get_start_date($post, false, $format = 'g:i a' ); ?> – <?php echo tribe_get_end_date($post, false, $format = 'g:i a' ); ?>
									</p>
									<a href="<?php the_permalink(); ?>" class="event-sign-up goudy">
										<em>Sign up now</em>
									</a>
								</div><!-- .media-body -->
							</div><!-- .media -->
						</div><!-- .event-full -->
						<?php wp_reset_postdata(); ?>
					<?php endif; ?>
				<?php endif; ?>
			<?php endwhile; ?>
			<?php else : ?>
		<?php endif; ?>

		<?php if( have_rows('event_boxes') ): ?>
			<?php while ( have_rows('event_boxes') ) : the_row(); ?>
				<?php if( get_row_layout() == 'event_2_col' ): ?>
					<?php $post_object = get_sub_field('event_2_1'); ?>
					<?php if( $post_object ): 
						// override $post
						$post = $post_object;
						setup_postdata( $post );
					?>
						<div class="event event-large">
							<div class="media gray first">
								<div class="media-left">
									<h4 class="month">
										<?php echo tribe_get_start_date($post, false, $format = 'F' ); ?>
									</h4>
									<h1 class="day">
										<?php echo tribe_get_start_date($post, false, $format = 'j' ); ?>
									</h1>
								</div><!-- .media-left -->
								<div class="media-body">
									<h4 class="media-heading">
										<a href="<?php the_permalink(); ?>" class="event-link">
											<?php the_title(); ?>
										</a>
									</h4>
									<p class="event-summary">
										<?php echo get_the_excerpt(); ?>
										<br />
										<?php echo tribe_get_start_date($post, false, $format = 'g:i a' ); ?> – <?php echo tribe_get_end_date($post, false, $format = 'g:i a' ); ?>
									</p>
									<a href="<?php the_permalink(); ?>" class="event-sign-up goudy">
										<em>Sign up now</em>
									</a>
								</div><!-- .media-body -->
							</div><!-- .media -->
						</div><!-- .event-large -->
						<?php wp_reset_postdata(); ?>
					<?php endif; ?>
				<?php endif; ?>
			<?php endwhile; ?>
			<?php else : ?>
		<?php endif; ?>

		<?php if( have_rows('event_boxes') ): ?>
			<?php while ( have_rows('event_boxes') ) : the_row(); ?>
				<?php if( get_row_layout() == 'event_2_col' ): ?>
					<?php $post_object = get_sub_field('event_2_2'); ?>
					<?php if( $post_object ): 
						// override $post
						$post = $post_object;
						setup_postdata( $post );
					?>
						<div class="event event-large">
							<div class="media gray last">
								<div class="media-left">
									<h4 class="month">
										<?php echo tribe_get_start_date($post, false, $format = 'F' ); ?>
									</h4>
									<h1 class="day">
										<?php echo tribe_get_start_date($post, false, $format = 'j' ); ?>
									</h1>
								</div><!-- .media-left -->
								<div class="media-body">
									<h4 class="media-heading">
										<a href="<?php the_permalink(); ?>" class="event-link">
											<?php the_title(); ?>
										</a>
									</h4>
									<p class="event-summary">
										<?php echo get_the_excerpt(); ?>
										<br />
										<?php echo tribe_get_start_date($post, false, $format = 'g:i a' ); ?> – <?php echo tribe_get_end_date($post, false, $format = 'g:i a' ); ?>
									</p>
									<a href="<?php the_permalink(); ?>" class="event-sign-up goudy">
										<em>Sign up now</em>
									</a>
								</div><!-- .media-body -->
							</div><!-- .media -->
						</div><!-- .event-large -->
						<?php wp_reset_postdata(); ?>
					<?php endif; ?>
				<?php endif; ?>
			<?php endwhile; ?>
			<?php else : ?>
				<div class="event event-large">
					<div class="media gray first" style="display: block; background: transparent !important; width: 100%; height: 1px !important;">
					</div><!-- .media -->
				</div><!-- .event -->
		<?php endif; ?>

		<?php if( have_rows('event_boxes') ): ?>
			<?php while ( have_rows('event_boxes') ) : the_row(); ?>
				<?php if( get_row_layout() == 'event_3_col' ): ?>
					<?php $post_object = get_sub_field('event_3_1'); ?>
					<?php if( $post_object ): 
						// override $post
						$post = $post_object;
						setup_postdata( $post );
					?>
						<div class="event event-three-col">
							<div class="media gray first">
								<div class="media-left">
									<h4 class="month">
										<?php echo tribe_get_start_date($post, false, $format = 'F' ); ?>
									</h4>
									<h1 class="day">
										<?php echo tribe_get_start_date($post, false, $format = 'j' ); ?>
									</h1>
								</div><!-- .media-left -->
								<div class="media-body">
									<h4 class="media-heading">
										<a href="<?php the_permalink(); ?>" class="event-link">
											<?php the_title(); ?>
										</a>
									</h4>
									<p class="event-summary">
										<?php echo get_the_excerpt(); ?>
										<br />
										<?php echo tribe_get_start_date($post, false, $format = 'g:i a' ); ?> – <?php echo tribe_get_end_date($post, false, $format = 'g:i a' ); ?>
									</p>
									<a href="<?php the_permalink(); ?>" class="event-sign-up goudy">
										<em>Sign up now</em>
									</a>
								</div><!-- .media-body -->
							</div><!-- .media -->
						</div><!-- .event-three-col -->
						<?php wp_reset_postdata(); ?>
					<?php endif; ?>
				<?php endif; ?>
			<?php endwhile; ?>
			<?php else : ?>
				<div class="event event-three-col">
					<div class="media gray first" style="display: block; background: transparent !important; width: 100%; height: 1px !important;">
					</div><!-- .media -->
				</div><!-- .event -->
		<?php endif; ?>

		<?php if( have_rows('event_boxes') ): ?>
			<?php while ( have_rows('event_boxes') ) : the_row(); ?>
				<?php if( get_row_layout() == 'event_3_col' ): ?>
					<?php $post_object = get_sub_field('event_3_2'); ?>
					<?php if( $post_object ): 
						// override $post
						$post = $post_object;
						setup_postdata( $post );
					?>
						<div class="event event-three-col">
							<div class="media gray second">
								<div class="media-left">
									<h4 class="month">
										<?php echo tribe_get_start_date($post, false, $format = 'F' ); ?>
									</h4>
									<h1 class="day">
										<?php echo tribe_get_start_date($post, false, $format = 'j' ); ?>
									</h1>
								</div><!-- .media-left -->
								<div class="media-body">
									<h4 class="media-heading">
										<a href="<?php the_permalink(); ?>" class="event-link">
											<?php the_title(); ?>
										</a>
									</h4>
									<p class="event-summary">
										<?php echo get_the_excerpt(); ?>
										<br />
										<?php echo tribe_get_start_date($post, false, $format = 'g:i a' ); ?> – <?php echo tribe_get_end_date($post, false, $format = 'g:i a' ); ?>
									</p>
									<a href="<?php the_permalink(); ?>" class="event-sign-up goudy">
										<em>Sign up now</em>
									</a>
								</div><!-- .media-body -->
							</div><!-- .media -->
						</div><!-- .event-three-col -->
						<?php wp_reset_postdata(); ?>
					<?php endif; ?>
				<?php endif; ?>
			<?php endwhile; ?>
			<?php else : ?>
				<div class="event event-three-col">
					<div class="media gray second" style="display: block; background: transparent !important; width: 100%; height: 1px !important;">
					</div><!-- .media -->
				</div><!-- .event -->
		<?php endif; ?>

		<?php if( have_rows('event_boxes') ): ?>
			<?php while ( have_rows('event_boxes') ) : the_row(); ?>
				<?php if( get_row_layout() == 'event_3_col' ): ?>
					<?php $post_object = get_sub_field('event_3_3'); ?>
					<?php if( $post_object ): 
						// override $post
						$post = $post_object;
						setup_postdata( $post );
					?>
						<div class="event event-three-col">
							<div class="media gray second">
								<div class="media-left">
									<h4 class="month">
										<?php echo tribe_get_start_date($post, false, $format = 'F' ); ?>
									</h4>
									<h1 class="day">
										<?php echo tribe_get_start_date($post, false, $format = 'j' ); ?>
									</h1>
								</div><!-- .media-left -->
								<div class="media-body">
									<h4 class="media-heading">
										<a href="<?php the_permalink(); ?>" class="event-link">
											<?php the_title(); ?>
										</a>
									</h4>
									<p class="event-summary">
										<?php echo get_the_excerpt(); ?>
										<br />
										<?php echo tribe_get_start_date($post, false, $format = 'g:i a' ); ?> – <?php echo tribe_get_end_date($post, false, $format = 'g:i a' ); ?>
									</p>
									<a href="<?php the_permalink(); ?>" class="event-sign-up goudy">
										<em>Sign up now</em>
									</a>
								</div><!-- .media-body -->
							</div><!-- .media -->
						</div><!-- .event-three-col -->
						<?php wp_reset_postdata(); ?>
					<?php endif; ?>
				<?php endif; ?>
			<?php endwhile; ?>
			<?php else : ?>
				<div class="event event-three-col">
					<div class="media gray last" style="display: block; background: transparent !important; width: 100%; height: 1px !important;">
					</div><!-- .media -->
				</div><!-- .event -->
		<?php endif; ?>

		<?php if( have_rows('event_boxes') ): ?>
			<?php while ( have_rows('event_boxes') ) : the_row(); ?>
				<?php if( get_row_layout() == 'event_4_col' ): ?>
					<?php $post_object = get_sub_field('event_4_1'); ?>
					<?php if( $post_object ): 
						// override $post
						$post = $post_object;
						setup_postdata( $post );
					?>
						<div class="event event-small">
							<div class="media gray first">
								<div class="media-left">
									<h4 class="month">
										<?php echo tribe_get_start_date($post, false, $format = 'F' ); ?>
									</h4>
									<h1 class="day">
										<?php echo tribe_get_start_date($post, false, $format = 'j' ); ?>
									</h1>
								</div><!-- .media-left -->
								<div class="media-body">
									<h4 class="media-heading">
										<a href="<?php the_permalink(); ?>" class="event-link">
											<?php the_title(); ?>
										</a>
									</h4>
									<p class="event-summary">
										<?php echo get_the_excerpt(); ?>
										<br />
										<?php echo tribe_get_start_date($post, false, $format = 'g:i a' ); ?> – <?php echo tribe_get_end_date($post, false, $format = 'g:i a' ); ?>
									</p>
								</div><!-- .media-body -->
							</div><!-- .media -->
						</div><!-- .event-small -->
						<?php wp_reset_postdata(); ?>
					<?php endif; ?>
				<?php endif; ?>
			<?php endwhile; ?>
			<?php else : ?>
				<div class="event event-small">
					<div class="media gray first" style="display: block; background: transparent !important; width: 100%; height: 1px !important;">
					</div><!-- .media -->
				</div><!-- .event -->
		<?php endif; ?>

		<?php if( have_rows('event_boxes') ): ?>
			<?php while ( have_rows('event_boxes') ) : the_row(); ?>
				<?php if( get_row_layout() == 'event_4_col' ): ?>
					<?php $post_object = get_sub_field('event_4_2'); ?>
					<?php if( $post_object ): 
						// override $post
						$post = $post_object;
						setup_postdata( $post );
					?>
						<div class="event event-small">
							<div class="media gray second">
								<div class="media-left">
									<h4 class="month">
										<?php echo tribe_get_start_date($post, false, $format = 'F' ); ?>
									</h4>
									<h1 class="day">
										<?php echo tribe_get_start_date($post, false, $format = 'j' ); ?>
									</h1>
								</div><!-- .media-left -->
								<div class="media-body">
									<h4 class="media-heading">
										<a href="<?php the_permalink(); ?>" class="event-link">
											<?php the_title(); ?>
										</a>
									</h4>
									<p class="event-summary">
										<?php echo get_the_excerpt(); ?>
										<br />
										<?php echo tribe_get_start_date($post, false, $format = 'g:i a' ); ?> – <?php echo tribe_get_end_date($post, false, $format = 'g:i a' ); ?>
									</p>
								</div><!-- .media-body -->
							</div><!-- .media -->
						</div><!-- .event-small -->
						<?php wp_reset_postdata(); ?>
					<?php endif; ?>
				<?php endif; ?>
			<?php endwhile; ?>
			<?php else : ?>
				<div class="event event-small">
					<div class="media gray second" style="display: block; background: transparent !important; width: 100%; height: 1px !important;">
					</div><!-- .media -->
				</div><!-- .event -->
		<?php endif; ?>

		<?php if( have_rows('event_boxes') ): ?>
			<?php while ( have_rows('event_boxes') ) : the_row(); ?>
				<?php if( get_row_layout() == 'event_4_col' ): ?>
					<?php $post_object = get_sub_field('event_4_3'); ?>
					<?php if( $post_object ): 
						// override $post
						$post = $post_object;
						setup_postdata( $post );
					?>
						<div class="event event-small">
							<div class="media gray third">
								<div class="media-left">
									<h4 class="month">
										<?php echo tribe_get_start_date($post, false, $format = 'F' ); ?>
									</h4>
									<h1 class="day">
										<?php echo tribe_get_start_date($post, false, $format = 'j' ); ?>
									</h1>
								</div><!-- .media-left -->
								<div class="media-body">
									<h4 class="media-heading">
										<a href="<?php the_permalink(); ?>" class="event-link">
											<?php the_title(); ?>
										</a>
									</h4>
									<p class="event-summary">
										<?php echo get_the_excerpt(); ?>
										<br />
										<?php echo tribe_get_start_date($post, false, $format = 'g:i a' ); ?> – <?php echo tribe_get_end_date($post, false, $format = 'g:i a' ); ?>
									</p>
								</div><!-- .media-body -->
							</div><!-- .media -->
						</div><!-- .event-small -->
						<?php wp_reset_postdata(); ?>
					<?php endif; ?>
				<?php endif; ?>
			<?php endwhile; ?>
			<?php else : ?>
				<div class="event event-small">
					<div class="media gray third" style="display: block; background: transparent !important; width: 100%; height: 1px !important;">
					</div><!-- .media -->
				</div><!-- .event -->
		<?php endif; ?>

		<?php if( have_rows('event_boxes') ): ?>
			<?php while ( have_rows('event_boxes') ) : the_row(); ?>
				<?php if( get_row_layout() == 'event_4_col' ): ?>
					<?php $post_object = get_sub_field('event_4_4'); ?>
					<?php if( $post_object ): 
						// override $post
						$post = $post_object;
						setup_postdata( $post );
					?>
						<div class="event event-small">
							<div class="media gray last">
								<div class="media-left">
									<h4 class="month">
										<?php echo tribe_get_start_date($post, false, $format = 'F' ); ?>
									</h4>
									<h1 class="day">
										<?php echo tribe_get_start_date($post, false, $format = 'j' ); ?>
									</h1>
								</div><!-- .media-left -->
								<div class="media-body">
									<h4 class="media-heading">
										<a href="<?php the_permalink(); ?>" class="event-link">
											<?php the_title(); ?>
										</a>
									</h4>
									<p class="event-summary">
										<?php echo get_the_excerpt(); ?>
										<br />
										<?php echo tribe_get_start_date($post, false, $format = 'g:i a' ); ?> – <?php echo tribe_get_end_date($post, false, $format = 'g:i a' ); ?>
									</p>
								</div><!-- .media-body -->
							</div><!-- .media -->
						</div><!-- .event-small -->
						<?php wp_reset_postdata(); ?>
					<?php endif; ?>
				<?php endif; ?>
			<?php endwhile; ?>
			<?php else : ?>
				<div class="event event-small">
					<div class="media gray last" style="display: block; background: transparent !important; width: 100%; height: 1px !important;">
					</div><!-- .media -->
				</div><!-- .event -->
		<?php endif; ?>
		<div class="clear"></div><!-- .clear -->
		<center>
			<a href="<?php echo esc_url( tribe_get_events_link() ); ?>" class="full-calendar-link goudy">
				<em>Full quarter calendar</em>
			</a>
		</center>
	</div><!-- .container -->
</div><!-- .events-wrap -->