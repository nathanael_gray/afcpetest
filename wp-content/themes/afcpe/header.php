<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
	<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
	<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
	<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?php wp_title('|', true, 'right'); ?></title>

		<meta property="og:url" content="<?php echo esc_url( home_url() ); ?>" />
		<meta property="og:type" content="website" />
		<meta property="og:title" content="<?php bloginfo('name'); ?>" />
		<meta property="og:description" content="<?php bloginfo('description'); ?>" />
		<meta property="og:image" content="<?php echo esc_url( home_url() ); ?>/wp-content/themes/afcpe/img/afcpe-logo-full.png" />

		<meta name="description" content="<?php bloginfo('description'); ?>">
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
			<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php endif; ?>
		<?php $pluginList = get_option( 'active_plugins' );?>
		<?php $plugin = 'advanced-custom-fields-pro/acf.php'; ?>
		<?php if ( in_array( $plugin , $pluginList ) ) { ?>
			<?php if( get_field('favicon', 'option') ) { ?>
				<link rel="icon" type="image/png" href="<?php the_field('favicon', 'option'); ?>">
			<?php } else {} ?>
		<?php } else {} ?>
		<?php wp_head(); ?>
	</head>
	<body <?php body_class('afcpe'); ?>>
		<header id="header">
			<div class="head-upper">
				<div class="container">
					<div class="left primary-nav-toggle">
						<a href="#" class="afcpe-nav nav-bars">
							Menu
						</a>
					</div><!-- .left -->
					<div class="middle">
						<div class="logo-wrap">
							<h1 id="logo">
								<a href="<?php echo esc_url( home_url() ); ?>" title="<?php wp_title('|', true, 'right'); ?>">
									<?php wp_title('|', true, 'right'); ?>
								</a>
							</h1>
						</div><!-- .logo-wrap -->
					</div><!-- .middle -->
					<div class="right">
						<div class="desktop">
							<?php get_template_part('blocks/navs/header', 'inner'); ?>
							<?php get_template_part('blocks/navs/header', 'login'); ?>
						</div><!-- .desktop -->
						<div class="mobile">
							<a href="#" class="login-link">
								Login
							</a>
						</div><!-- .mobile -->
					</div><!-- .right -->
					<div class="clear"></div><!-- .clear -->
				</div><!-- .inner -->
			</div><!-- .head-upper -->
			<div class="head-lower">
				<?php get_search_form(); ?>
				<?php get_template_part('blocks/content/mobile', 'search') ;?>
				<?php get_template_part('blocks/navs/nav', 'test2'); ?>
			</div><!-- .head-lower -->
		</header>
		<div class="overlay"></div>
		<div id="main-content">
			<?php get_template_part('blocks/content/subpage', 'header'); ?>