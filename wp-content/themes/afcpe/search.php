<?php get_header(); ?>
<div class="container">
	<h1>
		Showing search results for "<?php the_search_query(); ?>"
	</h1>
</div><!-- .container -->
<?php get_template_part('blocks/content/loop', 'archive'); ?>
<?php get_footer(); ?>