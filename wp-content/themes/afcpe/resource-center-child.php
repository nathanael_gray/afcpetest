<?php
/*
Template Name: Resource Center Child
*/
get_header(); ?>
<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="container">
			<article id="post-<?php the_ID(); ?>" class="entry archive-entry">
				<?php get_template_part('blocks/content/content', 'breadcrumbs'); ?>
				
				<div class="intro-copy">
					<?php the_field('intro_copy'); ?>
				</div><!-- .intro-copy -->

				<div class="resource-child-box-wrap">
					<?php if( get_field('box_1_title') ) { ?>
						<div class="col-md-4 box box-1">
							<h3 class="section-head goudy equal-height">
								<a href="<?php the_field('box_1_link'); ?>">
									<em><?php the_field('box_1_title'); ?></em>
								</a>
							</h3>
							<center>
								<a href="<?php the_field('box_1_link'); ?>">
									<img src="<?php the_field('box_1_image'); ?>" class="img-responsive" />
								</a>
							</center>
							<div class="box-content">
								<?php the_field('box_1_content'); ?>
							</div><!-- .box-content -->
						</div><!-- .col-md-4 -->
					<?php } else { ?>
						<div class="col-md-4 box box-1" style="height: 1px;">
						</div><!-- .col-md-4 -->
					<?php } ?>

					<?php if( get_field('box_2_title') ) { ?>
						<div class="col-md-4 box box-2">
							<h3 class="section-head goudy equal-height">
								<a href="<?php the_field('box_2_link'); ?>">
									<em><?php the_field('box_2_title'); ?></em>
								</a>
							</h3>
							<center>
								<a href="<?php the_field('box_2_link'); ?>">
									<img src="<?php the_field('box_2_image'); ?>" class="img-responsive" />
								</a>
							</center>
							<div class="box-content">
								<?php the_field('box_2_content'); ?>
							</div><!-- .box-content -->
						</div><!-- .col-md-4 -->
					<?php } else { ?>
						<div class="col-md-4 box box-2" style="height: 1px;">
						</div><!-- .col-md-4 -->
					<?php } ?>

					<?php if( get_field('box_3_title') ) { ?>
						<div class="col-md-4 box box-3">
							<h3 class="section-head goudy equal-height">
								<a href="<?php the_field('box_3_link'); ?>">
									<em><?php the_field('box_3_title'); ?></em>
								</a>
							</h3>
							<center>
								<a href="<?php the_field('box_3_link'); ?>">
									<img src="<?php the_field('box_3_image'); ?>" class="img-responsive" />
								</a>
							</center>
							<div class="box-content">
								<?php the_field('box_3_content'); ?>
							</div><!-- .box-content -->
						</div><!-- .col-md-4 -->
					<?php } else { ?>
						<div class="col-md-4 box box-3" style="height: 1px;">
						</div><!-- .col-md-4 -->
					<?php } ?>

					<div class="clear"></div><!-- .clear -->
				</div><!-- .resource-child-box-wrap -->

			</article><!-- .entry -->
		</div><!-- .container -->
	<?php endwhile; ?>
	<?php else : ?>
	<?php get_template_part('blocks/loops/loop', 'nocontent'); ?>
<?php endif; ?>

<?php get_template_part('blocks/content/subscribe', 'cta'); ?>
<?php get_footer(); ?>