			</div><!-- #main-content -->
			<?php get_template_part('blocks/content/footer', 'partners'); ?>
			<footer id="footer">
				<img src="<?php echo esc_url( home_url() ); ?>/wp-content/themes/afcpe/img/footer-logo.png" class="footer-logo" alt="<?php wp_title('|', true, 'right'); ?>" />
					<?php $pluginList = get_option( 'active_plugins' ); ?>
					<?php $plugin = 'advanced-custom-fields-pro/acf.php'; ?>
					<?php if ( in_array( $plugin , $pluginList ) ) { ?>
						<div class="footer-contact-wrap">
							<?php the_field('footer_contact_info', 'options'); ?>
						</div><!-- .footer-contact-wrap -->
					<?php } ?>
				<?php get_template_part('blocks/navs/nav', 'footer'); ?>
			</footer>
			<div class="container">
				<a href="#0" class="back-to-top">Back to Top</a>
			</div><!-- .container -->
		<?php wp_footer(); ?>
	</body>
</html>