<div class="search-wrap clear">
	<div class="container">
		<form id="searchform" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
			<input type="text" placeholder="search" value="" name="s" id="s" class="goudy" />
			<button type="submit">
				<span class="search-text">search</span><!-- .search-toggle -->
			</button>
		</form>
	</div><!-- .clear -->
</div><!-- .search-wrap -->