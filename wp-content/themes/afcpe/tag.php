<?php get_header(); ?>
<?php get_template_part('blocks/content/content', 'breadcrumbs'); ?>
<div class="container">
	<h1>
		<?php single_tag_title(); ?>
	</h1>
</div><!-- .container -->
<?php get_template_part('blocks/content/loop', 'archive'); ?>
<?php get_footer(); ?>