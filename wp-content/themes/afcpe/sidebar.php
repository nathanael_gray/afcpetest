<?php
	if ( is_page_template( 'right-sidebar.php' )  ) :
		get_template_part('blocks/sidebars/sidebar', 'right');
	endif;

	if ( is_page_template( 'left-sidebar.php' )  ) :
		get_template_part('blocks/sidebars/sidebar', 'left');
	endif;
?>