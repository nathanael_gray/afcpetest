<?php
/**
 * Default Events Template
 * This file is the basic wrapper template for all the views if 'Default Events Template'
 * is selected in Events -> Settings -> Template -> Events Template.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/default-template.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

get_header();
?>

<div class="subpage-head-wrap">
	<div class="container">
		<div class="inner">
			<h3 class="subpage-parents goudy">
				<em>Resource Center: Professional Development</em>
			</h3>
			<h1 class="subpage-title">
				Upcoming Programs
			</h1>
		</div>
	</div><!-- .container -->
</div><!-- .subpage-head-wrap -->

<div id="tribe-events-pg-template">
	<?php tribe_events_before_html(); ?>
	<?php tribe_get_view(); ?>
	<?php tribe_events_after_html(); ?>
</div> <!-- #tribe-events-pg-template -->
<?php get_template_part('blocks/content/subscribe', 'cta'); ?>
<?php
get_footer();
